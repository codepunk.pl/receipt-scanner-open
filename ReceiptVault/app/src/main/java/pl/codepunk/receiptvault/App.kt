package pl.codepunk.receiptvault

import androidx.appcompat.app.AppCompatDelegate
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.HasContentProviderInjector
import dagger.android.support.DaggerApplication
import kotlinx.android.synthetic.main.layout_settings.*
import pl.codepunk.receiptvault.preferences.PreferencesHelper

class App : DaggerApplication(), HasContentProviderInjector {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent.builder().application(this).build().apply {
            inject(this@App)
        }

    override fun onCreate() {
        super.onCreate()

        // LeakCanary
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }

        //LeakCanary.install(this)

        // TODO re work this
        AppCompatDelegate.setDefaultNightMode(
            if (PreferencesHelper(this).nightMode) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
    }
}