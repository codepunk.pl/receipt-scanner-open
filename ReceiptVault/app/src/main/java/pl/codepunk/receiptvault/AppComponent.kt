package pl.codepunk.receiptvault

import android.content.SharedPreferences
import dagger.Component
import dagger.android.AndroidInjector
import dagger.BindsInstance
import dagger.android.support.AndroidSupportInjectionModule
import pl.codepunk.receiptvault.processing.ai.AiReceiptModule
import pl.codepunk.receiptvault.processing.image.ImageModule
import pl.codepunk.receiptvault.processing.ocr.OcrModule
import pl.codepunk.receiptvault.permissions.PermissionsModule
import pl.codepunk.receiptvault.preferences.PreferencesModule
import pl.codepunk.receiptvault.repository.RepositoryModule
import pl.codepunk.receiptvault.session.SessionModule
import pl.codepunk.receiptvault.ui.activities.ActivityModule
import pl.codepunk.receiptvault.ui.fragments.FragmentModule
import pl.codepunk.receiptvault.ui.viewmodel.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        ViewModelModule::class,
        FragmentModule::class,
        ImageModule::class,
        AiReceiptModule::class,
        OcrModule::class,
        PermissionsModule::class,
        RepositoryModule::class,
        SessionModule::class,
        PreferencesModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }

}

