package pl.codepunk.receiptvault.permissions

import android.content.Context
import androidx.core.content.PermissionChecker
import androidx.core.content.PermissionChecker.PERMISSION_DENIED
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED

class PermissionsHelper(private val context: Context) {
    fun <T> onGranted(permission: String, function: () -> T): T = when (PermissionChecker.checkSelfPermission(context, permission)) {
        PERMISSION_GRANTED -> function()
        PERMISSION_DENIED -> throw MissingPermissionException(permission)
        else -> throw MissingPermissionException(permission)
    }

    fun isPermissionGranted(permission: String) = PermissionChecker.checkSelfPermission(context, permission) == PERMISSION_GRANTED
}

data class MissingPermissionException(val permission: String) : Throwable()
