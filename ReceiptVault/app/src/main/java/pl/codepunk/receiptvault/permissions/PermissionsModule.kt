package pl.codepunk.receiptvault.permissions

import dagger.Module
import dagger.Provides
import pl.codepunk.receiptvault.App
import javax.inject.Singleton

@Module
class PermissionsModule {

    @Provides
    @Singleton
    fun providePermissionsHelper(application: App): PermissionsHelper = PermissionsHelper(application)
}