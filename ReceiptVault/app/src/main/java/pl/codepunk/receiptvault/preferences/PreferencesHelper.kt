package pl.codepunk.receiptvault.preferences

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

private const val PREFS_FILENAME = " pl.codepunk.receiptvault.prefs"
private const val PASS_KEY = "pass_key"
private const val PASS_KEY_IV = "pass_key_iv"
private const val NIGHT_MODE_KEY = "night_mode_key"
private const val DISCLAIMER_KEY = "disclaimer_key"
private const val CONDITIONS_KEY = "conditions_key"

class PreferencesHelper @Inject constructor(context: Context) {
    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)

    var nightMode: Boolean
        get() = prefs.getBoolean(NIGHT_MODE_KEY, false)
        set(value) = prefs.edit().putBoolean(NIGHT_MODE_KEY, value).apply()

    var pass: String?
        get() = prefs.getString(PASS_KEY, null)
        set(value) = prefs.edit().putString(PASS_KEY, value).apply()

    var passIv: String?
        get() = prefs.getString(PASS_KEY_IV, null)
        set(value) = prefs.edit().putString(PASS_KEY_IV, value).apply()

    var disclaimer: Boolean
        get() = prefs.getBoolean(DISCLAIMER_KEY, false)
        set(value) = prefs.edit().putBoolean(DISCLAIMER_KEY, value).apply()

    var conditions: Boolean
        get() = prefs.getBoolean(CONDITIONS_KEY, false)
        set(value) = prefs.edit().putBoolean(CONDITIONS_KEY, value).apply()
}