package pl.codepunk.receiptvault.preferences

import dagger.Module
import dagger.Provides
import pl.codepunk.receiptvault.App
import javax.inject.Singleton

@Module
class PreferencesModule {

    @Provides
    @Singleton
    fun providePreferences(application: App) = PreferencesHelper(application)

}