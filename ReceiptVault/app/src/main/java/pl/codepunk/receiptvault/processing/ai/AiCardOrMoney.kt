package pl.codepunk.receiptvault.processing.ai

import android.content.Context
import org.tensorflow.lite.Interpreter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AiCardOrMoney @Inject constructor(context: Context) {

    private val labels: ArrayList<String> by lazy {
        context.assets.readFile("AiCardOrMoney.labels")
    }

    private val modelInterpreter: Interpreter by lazy {
        val options = Interpreter.Options()
        options.setNumThreads(2)
        Interpreter(context.assets.loadFile("AiCardOrMoney.tflite"), options)
    }

    fun predictCategory(text: String): Score {
        val outputData = TextInterpreterRunner.run(modelInterpreter, text, 2)
        return mapData(labels, outputData[0])
    }
}