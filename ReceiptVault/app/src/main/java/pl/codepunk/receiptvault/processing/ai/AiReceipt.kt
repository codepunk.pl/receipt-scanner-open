package pl.codepunk.receiptvault.processing.ai

import android.content.Context
import org.tensorflow.lite.Interpreter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AiReceipt @Inject constructor(context: Context) {

    private val labels: ArrayList<String> by lazy {
        context.assets.readFile("AiReceipt.labels")
    }

    private val modelInterpreter: Interpreter by lazy {
        val options = Interpreter.Options()
        options.setNumThreads(2)
        Interpreter(context.assets.loadFile("AiReceipt.tflite"), options)
    }

    fun predictCategory(index: SectionIndex, text: String): Score {
        val outputData = ReceiptInterpreterRunner.run(modelInterpreter, index, text)
        return mapData(labels, outputData[0])
    }
}