package pl.codepunk.receiptvault.processing.ai

import dagger.Module
import dagger.Provides
import pl.codepunk.receiptvault.App
import javax.inject.Singleton

@Module
class AiReceiptModule {

    @Singleton
    @Provides
    fun provideAiReceipt(appContext: App) = AiReceipt(appContext.applicationContext)

    @Singleton
    @Provides
    fun provideAiSection(appContext: App) = AiSection(appContext.applicationContext)

    @Singleton
    @Provides
    fun provideAiCardOrMoney(appContext: App) = AiCardOrMoney(appContext.applicationContext)

    @Singleton
    @Provides
    fun provideAiCurrency(appContext: App) = AiCurrency(appContext.applicationContext)

    @Singleton
    @Provides
    fun provideAiTime(appContext: App) = AiTime(appContext.applicationContext)

    @Singleton
    @Provides
    fun provideAiReceiptSectionLess(appContext: App) = AiReceiptSectionLess(appContext.applicationContext)
}