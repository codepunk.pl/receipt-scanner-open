package pl.codepunk.receiptvault.processing.ai

import android.content.Context
import org.tensorflow.lite.Interpreter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AiSection @Inject constructor(context: Context) {

    private val labels: ArrayList<String> by lazy {
        context.assets.readFile("AiSection.labels")
    }

    private val modelInterpreter: Interpreter by lazy {
        val options = Interpreter.Options()
        options.setNumThreads(2)
        Interpreter(context.assets.loadFile("AiSection.tflite"), options)
    }

    fun predictSection(rowOrderIndex: RowOrderIndex, rowsCount: RowCount): Score {
        val outputData = SectionInterpreterRunner.run(modelInterpreter, rowOrderIndex, rowsCount)
        return mapData(labels, outputData[0])
    }
}

