package pl.codepunk.receiptvault.processing.ai

import android.content.res.AssetManager
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.InputStreamReader
import java.math.BigDecimal
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel

typealias SectionIndex = FloatArray
typealias RowOrderIndex = Int
typealias RowCount = Int

fun AssetManager.loadFile(file: String): MappedByteBuffer {
    val fileDescriptor = openFd(file)
    val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
    val fileChannel = inputStream.channel
    val startOffset = fileDescriptor.startOffset
    val declaredLength = fileDescriptor.declaredLength
    return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
}

fun AssetManager.readFile(file: String): ArrayList<String> {
    val labelList = ArrayList<String>()
    val reader = BufferedReader(InputStreamReader(open(file)))
    while (true) {
        val line = reader.readLine() ?: break
        labelList.add(line)
    }
    reader.close()
    return labelList
}


fun round(d: Float, decimalPlace: Int): Float {
    var bd = BigDecimal(d.toString())
    bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP)
    return bd.toFloat()
}

typealias Score = Map<String, Float>

fun mapData(labels: ArrayList<String>, floats: FloatArray): Map<String, Float> {
    if (labels.size != floats.size) {
        return emptyMap()
    }

    val map: MutableMap<String, Float> = mutableMapOf()

    for (i in floats.indices) {
        map[labels[i]] = round(floats[i], 2)
    }

    return map
}