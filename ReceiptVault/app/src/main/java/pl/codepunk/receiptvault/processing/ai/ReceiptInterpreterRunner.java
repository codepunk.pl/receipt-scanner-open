package pl.codepunk.receiptvault.processing.ai;

import org.tensorflow.lite.Interpreter;

import java.util.Arrays;

public class ReceiptInterpreterRunner {

    static float[][] run(Interpreter modelInterpreter, float[] sectionScore, String text) {
        String newString = text.replace("\ufeff", "").toUpperCase();
        float[] inpArray = new float[67];
        Arrays.fill(inpArray, 0);

        inpArray[64] = sectionScore[0];
        inpArray[65] = sectionScore[1];
        inpArray[66] = sectionScore[2];

        char[] inpCharArray = newString.toCharArray();

        for (int i = 0; i < inpCharArray.length; i++) {
            if (i >= inpArray.length) {
                break;
            }

            inpArray[i] = (float) inpCharArray[i];
        }

        float[][] inp = new float[][]{inpArray};
        float[][] out = new float[][]{new float[10]};
        modelInterpreter.run(inp, out);
        return out;
    }
}

