package pl.codepunk.receiptvault.processing.ai;

import org.tensorflow.lite.Interpreter;

public class SectionInterpreterRunner {
    static float[][] run(Interpreter modelInterpreter, int sectionIndex, int sectionCount) {
        float[] inpArray = {sectionIndex, sectionCount};
        float[][] inp = new float[][]{inpArray};
        float[][] out = new float[][]{new float[3]};
        modelInterpreter.run(inp, out);
        return out;
    }
}
