package pl.codepunk.receiptvault.processing.ai;

import org.tensorflow.lite.Interpreter;

import java.util.Arrays;

public class TextInterpreterRunner {

    static float[][] run(Interpreter modelInterpreter, String text, int shape) {
        String newString = text.replace("\ufeff", "").toUpperCase();
        float[] inpArray = new float[67];
        Arrays.fill(inpArray, 0);

        char[] inpCharArray = newString.toCharArray();

        for (int i = 0; i < inpCharArray.length; i++) {
            if (i >= inpArray.length) {
                break;
            }

            inpArray[i] = (float) inpCharArray[i];
        }

        float[][] inp = new float[][]{inpArray};
        float[][] out = new float[][]{new float[shape]};
        modelInterpreter.run(inp, out);
        return out;
    }
}

