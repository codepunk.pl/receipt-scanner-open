package pl.codepunk.receiptvault.processing.image

import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ImageCache @Inject constructor() {
    private val cachedImages: MutableMap<String, CachedImageData> = mutableMapOf()

    fun putIntoCache(image: ByteArray, rotationDegrees: Int, width: Int, height: Int): ImageCachedToken {
        val id = UUID.randomUUID().toString()
        cachedImages[id] = CachedImageData(image, rotationDegrees, width, height)
        return id
    }

    fun getAndClearFromCache(token: ImageCachedToken) : CachedImageData? {
        return cachedImages.remove(token)
    }

    fun clearCache() {
        cachedImages.clear()
    }
}

data class CachedImageData(@Suppress("ArrayInDataClass") val image: ByteArray, val rotationDegrees: Int, val width: Int, val height: Int)
typealias ImageCachedToken = String