package pl.codepunk.receiptvault.processing.image

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ImageModule {

    @Singleton
    @Provides
    fun provideImageCache(): ImageCache = ImageCache()
}