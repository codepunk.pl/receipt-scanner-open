package pl.codepunk.receiptvault.processing.ocr

import com.google.android.gms.vision.text.TextRecognizer
import dagger.Module
import dagger.Provides
import pl.codepunk.receiptvault.App

@Module
class OcrModule {

    @Provides
    fun provideOcrBuilder(context: App) = TextRecognizer.Builder(context.applicationContext)
}