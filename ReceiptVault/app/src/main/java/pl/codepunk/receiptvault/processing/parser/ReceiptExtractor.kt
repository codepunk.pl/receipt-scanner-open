package pl.codepunk.receiptvault.processing.parser

import android.util.Log
import org.joda.money.Money
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat
import pl.codepunk.receiptvault.ui.viewmodel.ProcessedText
import java.lang.StringBuilder

fun extractReceipt(processedTexts: List<ProcessedText>): ProcessedReceipt {
    // Find NAMES
    val firmy = findAll(processedTexts, "FIRMA")
    // Find ADDRESSES
    val adresy = findAll(processedTexts, "ADRES")
    // Find DATES
    val daty = findAll(processedTexts, "DATA")
    // FIND PRICE TAGS
    val infoSumy = findAll(processedTexts, "INFO-SUMA")
    // FIND PRICES
    val sumy = findAll(processedTexts, "SUMA")
    // FIND PAYMENT METHOD
    val platnosci = findAll(processedTexts, "INFO-PLATNOSC")
    // FIND RELATED PRICE TAGS -> PRICES
    val infoSumySumy = findRelated(infoSumy, sumy)

    return ProcessedReceipt(
        firmy = firmy,
        adresy = adresy,
        daty = daty,
        sumy = infoSumySumy,
        platnosci = platnosci
    )
}

data class ProcessedReceipt(
    val firmy: List<ProcessedText>,
    val adresy: List<ProcessedText>,
    val daty: List<ProcessedText>,
    val sumy: List<ProcessedText>,
    val platnosci: List<ProcessedText>
)

fun List<String>.merge(last: Int = 2): String {
    val textBuilder = StringBuilder()
    takeLast(last).forEach {
        textBuilder.append(it.replace("\\s+".toRegex(), " ")).append(" ")
    }

    return textBuilder.toString().trimEnd()
}

fun findRelated(infoSumy: List<ProcessedText>, sumy: List<ProcessedText>): List<ProcessedText> =
    sumy.filter { suma ->
        infoSumy.filter { tagSumy ->
            suma.line.boundingBox.centerY() > tagSumy.line.boundingBox.top && suma.line.boundingBox.centerY() < tagSumy.line.boundingBox.bottom
        }.isNotEmpty()
    }

fun findAll(processedTexts: List<ProcessedText>, label: String): List<ProcessedText> =
    processedTexts.filter {
        it.categories.filter { catScore ->
            catScore.value > 0.8f && catScore.key == label
        }.isNotEmpty()
    }.sortedBy {
        it.line.boundingBox.centerY()
    }

fun interpretDate(date: String?): LocalDate? {
    return skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("M/d/yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("MM/dd/yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("MM/dd/yyyy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("yy/MM/dd")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("yyyy/MM/dd")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("dd/MMM/yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("dd/MMM/yyyy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("dd/MM/yyyy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("M-d-yyyy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("M-d-yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("MM-dd-yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("MM-dd-yyyy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("yy-MM-dd")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("yyyy-MM-dd")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("dd-MMM-yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("dd-MM-yyyy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("MM/dd/yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("M.d.yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("MM.dd.yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("MM.dd.yyyy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("yy.MM.dd")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("yyyy.MM.dd")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("dd.MMM.yy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("dd.MMM.yyyy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("dd.MM.yyyy")) }
        ?: skipCrash { LocalDate.parse(date, DateTimeFormat.forPattern("M.d.yyyy")) }
}

fun interpretTime(time: String?): LocalTime? {
    return skipCrash { LocalTime.parse(time, DateTimeFormat.forPattern("hh:mm:ss")) }
        ?: skipCrash { LocalTime.parse(time, DateTimeFormat.forPattern("hh:mm")) }
}

fun parseMoney(money: String?): String? {
    return try {
        return Money.parse(money?.replace(",", ".")?.trim())?.toString()
    } catch (t: Throwable) {
        null
    }
}

fun parseName(name: String?): String? {
    if (name == null || name.isBlank()) return null
    return name
}

fun <T> skipCrash(function: () -> T): T? {
    return try {
        function()
    } catch (t: Throwable) {
        null
    }
}

