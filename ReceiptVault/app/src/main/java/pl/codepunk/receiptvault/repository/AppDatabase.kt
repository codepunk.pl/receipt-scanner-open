package pl.codepunk.receiptvault.repository

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.commonsware.cwac.saferoom.SafeHelperFactory
import pl.codepunk.receiptvault.App
import pl.codepunk.receiptvault.repository.dao.ReceiptDao
import pl.codepunk.receiptvault.repository.model.ReceiptEntry
import pl.codepunk.receiptvault.session.Password
import pl.codepunk.receiptvault.session.SessionManager
import javax.inject.Inject

@Database(entities = [ReceiptEntry::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun receiptDao(): ReceiptDao
}

class AppDatabaseFactory @Inject constructor(val application: App) {

    fun openSecureDb(password: Password): AppDatabase {
        val factory = SafeHelperFactory(password.toByteArray())

        return Room.databaseBuilder(
            application,
            AppDatabase::class.java, "app-db"
        ).openHelperFactory(factory).build()
    }
}