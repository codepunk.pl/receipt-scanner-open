package pl.codepunk.receiptvault.repository

import pl.codepunk.receiptvault.App
import pl.codepunk.receiptvault.repository.model.ReceiptEntry
import pl.codepunk.receiptvault.session.SessionManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReceiptRepository @Inject constructor(
    private val databaseFactory: AppDatabaseFactory,
    private val sessionManager: SessionManager,
    private val app: App
) {
    var database: AppDatabase? = refreshDatabase()

    fun reInitRepository() = database?.let { db ->
        db.clearAllTables()
        db.close()
        app.deleteDatabase(db.openHelper.databaseName)
    }.run {
        database = refreshDatabase()
    }

    private fun refreshDatabase(): AppDatabase? =
        sessionManager.getPasswordUnsafe()?.let { databaseFactory.openSecureDb(it) }

    fun storeReceipt(
        companyName: String,
        companyAddress: String,
        receiptDate: Long,
        receiptTotalSum: String,
        receiptPaymentType: Int,
        imageId: String
    ): Boolean {
        return database?.receiptDao()?.insert(
            ReceiptEntry(
                owner = companyName,
                date = receiptDate,
                file = imageId,
                address = companyAddress,
                paymentType = receiptPaymentType,
                price = receiptTotalSum
            )
        )?.let { true } ?: false
    }

    fun updateReceipt(
        companyName: String,
        companyAddress: String,
        receiptDate: Long,
        receiptTotalSum: String,
        receiptPaymentType: Int,
        imageId: String,
        id: Long
    ): Boolean {
        return database?.receiptDao()?.update(
            ReceiptEntry(
                owner = companyName,
                date = receiptDate,
                file = imageId,
                address = companyAddress,
                paymentType = receiptPaymentType,
                price = receiptTotalSum
            ).apply {
                this.id = id
            }
        )?.let { true } ?: false
    }

    fun size() = app.getDatabasePath(database?.openHelper?.databaseName)?.length() ?: 0L
}