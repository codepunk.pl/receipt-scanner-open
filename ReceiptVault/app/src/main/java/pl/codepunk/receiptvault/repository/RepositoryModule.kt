package pl.codepunk.receiptvault.repository

import dagger.Module
import dagger.Provides
import pl.codepunk.receiptvault.App
import pl.codepunk.receiptvault.repository.archive.ImageRepository
import pl.codepunk.receiptvault.repository.feed.ReceiptsFeedFactory
import pl.codepunk.receiptvault.session.SessionManager
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideDatabase(application: App): AppDatabaseFactory = AppDatabaseFactory(application)

    @Provides
    @Singleton
    fun provideImageRepository(application: App, sessionManager: SessionManager): ImageRepository =
        ImageRepository(application, sessionManager)

    @Provides
    @Singleton
    fun provideReceiptRepository(
        appDatabaseFactory: AppDatabaseFactory,
        application: App,
        sessionManager: SessionManager
    ): ReceiptRepository =
        ReceiptRepository(appDatabaseFactory, sessionManager, application)

    @Provides
    @Singleton
    fun provideReceiptFeedFactory(receiptRepository: ReceiptRepository) =
        ReceiptsFeedFactory(receiptRepository)
}