package pl.codepunk.receiptvault.repository.archive

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.util.Log
import net.lingala.zip4j.core.ZipFile
import net.lingala.zip4j.model.FileHeader
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.util.Zip4jConstants
import pl.codepunk.receiptvault.session.SessionManager
import java.io.ByteArrayInputStream
import java.io.File
import java.lang.IllegalStateException
import java.util.*
import javax.inject.Inject

private const val RECEIPTS_IMG_DIR = "receipts"
private const val RECEIPTS_IMG_ZIP = "receipts.zip"

class ImageRepository @Inject constructor(val context: Context, private val sessionManager: SessionManager) {

    private val file: File = File(context.getDir(RECEIPTS_IMG_DIR, MODE_PRIVATE), RECEIPTS_IMG_ZIP)

    fun store(byteArray: ByteArray): String = sessionManager.getPasswordUnsafe()?.let { password ->
        if (count() == 0) {
            file.delete()
        }

        val zipParam = ZipParameters()
        zipParam.isSourceExternalStream = true
        zipParam.isEncryptFiles = true
        zipParam.encryptionMethod = Zip4jConstants.ENC_METHOD_AES
        zipParam.aesKeyStrength = Zip4jConstants.AES_STRENGTH_256
        zipParam.setPassword(password)

        val zipFile = ZipFile(file)
        var fileName: String

        while (true) {
            fileName = "${UUID.randomUUID()}.jpg"
            zipParam.fileNameInZip = fileName

            if (!zipFile.isValidZipFile) {
                zipFile.addStream(ByteArrayInputStream(byteArray), zipParam)
                break
            }

            val exists = zipFile.fileHeaders.find { it is FileHeader && it.fileName.contains(fileName) }
            if (exists == null) {
                zipFile.addStream(ByteArrayInputStream(byteArray), zipParam)
                break
            }
        }

        return fileName
    } ?: throw IllegalStateException()

    fun retrieve(fileName: String): ByteArray? = sessionManager.getPassword()?.let { password ->
        val zipFile = ZipFile(file)
        zipFile.setPassword(password)
        return zipFile.getFileHeader(fileName)?.let { zipFile.getInputStream(it) }?.readBytes()
    } ?: throw IllegalStateException()

    fun remove(fileName: String) = sessionManager.getPassword()?.let { password ->
        val zipParam = ZipParameters()
        zipParam.isSourceExternalStream = true
        zipParam.isEncryptFiles = true
        zipParam.encryptionMethod = Zip4jConstants.ENC_METHOD_AES
        zipParam.aesKeyStrength = Zip4jConstants.AES_STRENGTH_256
        zipParam.setPassword(password)

        val zipFile = ZipFile(file)
        zipFile.removeFile(fileName)
    } ?: throw IllegalStateException()

    fun reInitRepository() = file.delete()

    fun size() = file.length()

    fun count() = sessionManager.getPasswordUnsafe()?.let { password ->
        val zipParam = ZipParameters()
        zipParam.isSourceExternalStream = true
        zipParam.isEncryptFiles = true
        zipParam.encryptionMethod = Zip4jConstants.ENC_METHOD_AES
        zipParam.aesKeyStrength = Zip4jConstants.AES_STRENGTH_256
        zipParam.setPassword(password)

        if (!file.exists()) return 0

        try {
            val zipFile = ZipFile(file)
            zipFile.fileHeaders?.size
        } catch (e: Throwable) {
            return 0
        }
    } ?: throw IllegalStateException()
}