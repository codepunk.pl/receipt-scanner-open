package pl.codepunk.receiptvault.repository.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import pl.codepunk.receiptvault.repository.model.ReceiptEntry

@Dao
interface ReceiptDao {
    @Query("SELECT * from receipt_table ORDER BY date ASC")
    fun getAll(): List<ReceiptEntry>

    @Query("SELECT * from receipt_table WHERE id is :id")
    fun get(id: Long): ReceiptEntry

    @Insert
    fun insert(receiptEntry: ReceiptEntry)

    @Update
    fun update(receiptEntry: ReceiptEntry)

    @Query("DELETE FROM receipt_table WHERE id IS :id")
    fun delete(id: Long)
}