package pl.codepunk.receiptvault.repository.feed

import androidx.paging.DataSource
import pl.codepunk.receiptvault.repository.ReceiptRepository
import pl.codepunk.receiptvault.repository.model.ReceiptEntry
import javax.inject.Inject

class ReceiptsFeedFactory @Inject constructor(private val receiptRepository: ReceiptRepository) : DataSource.Factory<Long, ReceiptEntry>() {
    override fun create(): DataSource<Long, ReceiptEntry> {
        return ReceiptsFeedSource(receiptRepository)
    }
}