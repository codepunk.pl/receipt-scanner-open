package pl.codepunk.receiptvault.repository.feed

import android.util.Log
import androidx.paging.PageKeyedDataSource
import pl.codepunk.receiptvault.repository.ReceiptRepository
import pl.codepunk.receiptvault.repository.model.ReceiptEntry

class ReceiptsFeedSource(private val receiptRepository: ReceiptRepository) : PageKeyedDataSource<Long, ReceiptEntry>() {

    override fun loadInitial(
        params: LoadInitialParams<Long>,
        callback: LoadInitialCallback<Long, ReceiptEntry>
    ) {
        receiptRepository.database?.let {db ->
            callback.onResult(db.receiptDao().getAll(), null, 1)
        }
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, ReceiptEntry>) {
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, ReceiptEntry>) {
    }
}