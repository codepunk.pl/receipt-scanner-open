package pl.codepunk.receiptvault.repository.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "receipt_table")
data class ReceiptEntry(
    @ColumnInfo(name = "owner") val owner: String,
    @ColumnInfo(name = "date") val date: Long,
    @ColumnInfo(name = "file") val file: String,
    @ColumnInfo(name = "address") val address: String,
    @ColumnInfo(name = "price") val price: String,
    @ColumnInfo(name = "paymentType") val paymentType: Int
) : Parcelable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0
}