package pl.codepunk.receiptvault.session

import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import pl.codepunk.receiptvault.preferences.PreferencesHelper
import java.security.KeyStore
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec

internal class Security(private val prefs: PreferencesHelper) {

    internal fun setAppPassword(password: Password, secretKey: SecretKey) {
        val credentials = generateAppPassword(password, secretKey)
        prefs.pass = String(credentials.first)
        prefs.passIv = String(credentials.second)
    }

    internal fun getAppPassword(secretKey: SecretKey): ByteArray {
        if (prefs.pass == null || prefs.passIv == null) {
            return ByteArray(0)
        }

        val encryptedPass = prefs.pass
        val encryptedPassIv = prefs.passIv

        val cipher = Cipher.getInstance("AES/GCM/NoPadding").apply {
            val spec = GCMParameterSpec(128, Base64.decode(encryptedPassIv, Base64.DEFAULT))
            init(Cipher.DECRYPT_MODE, secretKey, spec)
        }

        return cipher.doFinal(Base64.decode(encryptedPass, Base64.DEFAULT))
    }

    internal fun getSecretKey(): SecretKey {
        val keyStore: KeyStore by lazy {
            KeyStore.getInstance("AndroidKeyStore").apply {
                load(null)
            }
        }

        val keyGenerator: KeyGenerator by lazy {
            KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore").apply {
                val keyGenParameterSpec =
                    KeyGenParameterSpec.Builder(
                        "APP_KEY",
                        KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                    )
                        .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                        .build()

                init(keyGenParameterSpec)
            }
        }

        return (keyStore.getEntry("APP_KEY", null) as? KeyStore.SecretKeyEntry)?.secretKey ?: run {
            keyGenerator.generateKey()
        }
    }

    internal fun removeSecretKey() {
        val keyStore: KeyStore by lazy {
            KeyStore.getInstance("AndroidKeyStore").apply {
                load(null)
            }
        }

        keyStore.deleteEntry("APP_KEY")
        prefs.pass = ""
        prefs.passIv = ""
    }

    private fun generateAppPassword(password: Password, secretKey: SecretKey): Pair<ByteArray, ByteArray> {
        val cipher = Cipher.getInstance("AES/GCM/NoPadding").apply {
            init(Cipher.ENCRYPT_MODE, secretKey)
        }

        return Pair(
            Base64.encode(cipher.doFinal(password.toByteArray()), Base64.DEFAULT),
            Base64.encode(cipher.iv, Base64.DEFAULT)
        )
    }
}

typealias Password = String