package pl.codepunk.receiptvault.session

import pl.codepunk.receiptvault.preferences.PreferencesHelper
import java.lang.IllegalArgumentException
import javax.inject.Inject

class SessionManager @Inject constructor(preferences: PreferencesHelper) {

    private val security: Security = Security(preferences)
    private var accountActive = false

    fun isActive() = accountActive

    fun activate(password: Password) = security.getSecretKey().let { secretKey ->
        if (password == String(security.getAppPassword(secretKey))) accountActive = true
    }

    fun reset() {
        accountActive = false
        security.removeSecretKey()
    }

    fun isPasswordPresent(): Boolean = security.getAppPassword(security.getSecretKey()).isNotEmpty()

    fun getPassword(): Password? = when (isActive()) {
        true -> String(security.getAppPassword(security.getSecretKey()))
        false -> null
    }

    fun getPasswordUnsafe(): Password? = String(security.getAppPassword(security.getSecretKey()))

    fun setPassword(password: Password) = verifyPassword(password) {
        security.getSecretKey().let { secretKey ->
            security.setAppPassword(password, secretKey)
            activate(password)
        }
    }

    private fun verifyPassword(password: Password, function: () -> Unit) {
        if (password.length > 4) function() else throw IllegalArgumentException()
    }
}

