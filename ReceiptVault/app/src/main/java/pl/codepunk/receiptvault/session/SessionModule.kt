package pl.codepunk.receiptvault.session

import dagger.Module
import dagger.Provides
import pl.codepunk.receiptvault.preferences.PreferencesHelper
import javax.inject.Singleton

@Module
class SessionModule {

    @Provides
    @Singleton
    fun provideSessionManager(preferencesHelper: PreferencesHelper) = SessionManager(preferencesHelper)
}