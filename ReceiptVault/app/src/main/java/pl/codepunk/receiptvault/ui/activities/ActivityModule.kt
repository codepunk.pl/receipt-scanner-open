package pl.codepunk.receiptvault.ui.activities

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun addReceiptActivity(): AddReceiptActivity

    @ContributesAndroidInjector
    abstract fun editResultActivity(): EditResultActivity

    @ContributesAndroidInjector
    abstract fun contentActivity(): ContentActivity
}