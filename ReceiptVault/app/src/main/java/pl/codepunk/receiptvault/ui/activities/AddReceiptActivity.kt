package pl.codepunk.receiptvault.ui.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.databinding.LayoutAddReceiptBinding
import pl.codepunk.receiptvault.ui.viewmodel.AddReceiptViewModel
import pl.codepunk.receiptvault.utils.ScopedActivity
import pl.codepunk.receiptvault.utils.hideStatusUI
import pl.codepunk.receiptvault.utils.showStatusUI
import javax.inject.Inject

class AddReceiptActivity : ScopedActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AddReceiptViewModel
    private lateinit var binding: LayoutAddReceiptBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AddReceiptViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.layout_add_receipt)
        binding.vm = viewModel

        setTheme(R.style.AppTheme_Dark)
    }

    override fun onResume() {
        super.onResume()
        hideStatusUI()
    }

    override fun onPause() {
        super.onPause()
        showStatusUI()
    }

}
