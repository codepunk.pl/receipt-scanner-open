package pl.codepunk.receiptvault.ui.activities

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import kotlinx.android.synthetic.main.layout_content.*
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.utils.ScopedActivity

class ContentActivity : ScopedActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_content)

        when (AppCompatDelegate.getDefaultNightMode()) {
            AppCompatDelegate.MODE_NIGHT_YES -> this.let { act ->
                act.setTheme(R.style.AppTheme_Dark)

                act.window?.let { window ->
                    window.statusBarColor = resources.getColor(R.color.background, null)
                    window.decorView.systemUiVisibility = window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                    window.setBackgroundDrawable(ColorDrawable(window.statusBarColor))
                }
            }
            else -> this.let  {act ->
                act.setTheme(R.style.AppTheme_Light)

                act.window?.let { window ->
                    window.statusBarColor = resources.getColor(R.color.light_background, null)
                    window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    window.setBackgroundDrawable(ColorDrawable(window.statusBarColor))
                }
            }
        }

        when (intent?.extras?.getString("TYPE")) {
            "TERMS" -> webview.loadUrl("file:///android_asset/license/terms_and_conditions.html")
            "POLICY" -> webview.loadUrl("file:///android_asset/license/privacy_policy.html")
            "ACKNOWLEDGEMENTS" -> webview.loadUrl("file:///android_asset/license/acknowledgements.html")
            "HELP" -> webview.loadUrl("file:///android_asset/license/help.html")
            else -> finish()
        }
    }

    companion object {
        fun makeTermsAndConditions(context: Context): Intent = Intent(context, ContentActivity::class.java).apply {
            putExtra("TYPE", "TERMS")
        }

        fun makePrivacyPolicy(context: Context): Intent = Intent(context, ContentActivity::class.java).apply {
            putExtra("TYPE", "POLICY")
        }

        fun makeAcknowledgements(context: Context): Intent = Intent(context, ContentActivity::class.java).apply {
            putExtra("TYPE", "ACKNOWLEDGEMENTS")
        }

        fun makeHelp(context: Context): Intent = Intent(context, ContentActivity::class.java).apply {
            putExtra("TYPE", "HELP")
        }

        fun makeEmpty(context: Context): Intent = Intent(context, ContentActivity::class.java)
    }
}
