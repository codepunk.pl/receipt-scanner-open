package pl.codepunk.receiptvault.ui.activities

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_edit_result.*
import kotlinx.android.synthetic.main.layout_edit_result_item.*
import kotlinx.coroutines.launch
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.processing.ai.readFile
import pl.codepunk.receiptvault.ui.viewmodel.ProcessedText
import pl.codepunk.receiptvault.utils.ScopedActivity
import java.io.File

class EditResultActivity : ScopedActivity() {
    private lateinit var pathToStore: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_edit_result)

        pathToStore = Environment.getExternalStorageDirectory().absolutePath + File.separatorChar + packageName + File.separatorChar
        File(pathToStore).mkdirs()

        val text = intent.getParcelableExtra("TEXT_TO_FIX") as ProcessedText
        labelToChange.text = text.line.value

        options.layoutManager = LinearLayoutManager(this)
        options.adapter = CategoriesAdapter(this)

        buttonToChangeHeader.setOnClickListener {
            launch {
                (options.adapter as CategoriesAdapter).getSelectedItems().let { labels ->
                    for(label in labels) {
                        val fileToStoreData = "$pathToStore$label.csv"
                        File(fileToStoreData).appendText("\"${text.line}\",\"$label\",\"HEADER\",\"${text.index}\",\"${text.size}\"\n")
                    }
                    finish()
                }
            }
        }

        buttonToChangeBody.setOnClickListener {
            launch {
                (options.adapter as CategoriesAdapter).getSelectedItems().let { labels ->
                    for(label in labels) {
                        val fileToStoreData = "$pathToStore$label.csv"
                        File(fileToStoreData).appendText("\"${text.line}\",\"$label\",\"BODY\",\"${text.index}\",\"${text.size}\"\n")
                    }
                    finish()
                }
            }
        }

        buttonToChangeTail.setOnClickListener {
            launch {
                (options.adapter as CategoriesAdapter).getSelectedItems().let { labels ->
                    for(label in labels) {
                        val fileToStoreData = "$pathToStore$label.csv"
                        File(fileToStoreData).appendText("\"${text.line}\",\"$label\",\"TAIL\",\"${text.index}\",\"${text.size}\"\n")
                    }
                    finish()
                }
            }
        }
    }
}

class CategoriesAdapter(context: Context) : RecyclerView.Adapter<StringViewHolder>() {

    private val selectedItems: MutableList<Int> = mutableListOf()

    private val list: ArrayList<String> by lazy {
        context.assets.readFile("AiReceipt.labels")
    }

    fun getSelectedItems(): List<String> {
        return selectedItems.map { list[it] }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringViewHolder =
        StringViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_edit_result_item, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: StringViewHolder, position: Int) =
        holder.bind(list[position], selectedItems.contains(position)) {

            if (selectedItems.contains(it)) {
                selectedItems.remove(it)
            } else {
                selectedItems.add(it)
            }

            notifyDataSetChanged()
        }
}

class StringViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(value: String, selected: Boolean, function: (position: Int) -> Unit) {
        if (selected) {
            text.setBackgroundColor(Color.LTGRAY)
        } else {
            text.setBackgroundColor(Color.WHITE)
        }

        text.text = value
        text.setOnClickListener {
            function(adapterPosition)
        }
    }
}