package pl.codepunk.receiptvault.ui.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.databinding.LayoutMainBinding
import pl.codepunk.receiptvault.ui.viewmodel.MainViewModel
import pl.codepunk.receiptvault.utils.ScopedActivity
import javax.inject.Inject

class MainActivity : ScopedActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: LayoutMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.layout_main)
        binding.vm = viewModel
    }
}