package pl.codepunk.receiptvault.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.repository.model.ItemEntry

class ReceiptItemsAdapter : OpenListAdapter<ItemEntry, ReceiptItemEntryViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReceiptItemEntryViewHolder = ReceiptItemEntryViewHolder.create(parent)

    override fun onBindViewHolder(holder: ReceiptItemEntryViewHolder, position: Int) = getItem(position).let { holder.bind(it) }
}

private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ItemEntry>() {
    override fun areItemsTheSame(
        old: ItemEntry,
        new: ItemEntry
    ) = old.id == new.id

    override fun areContentsTheSame(
        old: ItemEntry,
        new: ItemEntry
    ) = old == new
}

class ReceiptItemEntryViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(item: ItemEntry?) {

    }

    companion object {
        fun create(parent: ViewGroup) = ReceiptItemEntryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_entry_item,
                parent,
                false
            )
        )
    }
}