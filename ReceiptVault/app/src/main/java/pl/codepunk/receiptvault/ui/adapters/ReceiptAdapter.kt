package pl.codepunk.receiptvault.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_entry_receipt.*
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.repository.model.ReceiptEntry
import java.text.SimpleDateFormat
import java.util.*

class DashboardReceiptAdapter(private val onClick: (ReceiptEntry) -> Unit): OpenListAdapter<ReceiptEntry, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = ReceiptEntryViewHolder.create(this, parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {}

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        when (holder) {
            is ReceiptEntryViewHolder -> getItem(position).let { holder.bind(it,payloads.firstOrNull() as? Boolean ?: false, onClick) }
        }

        super.onBindViewHolder(holder, position, payloads)
    }
}

private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ReceiptEntry>() {
    override fun areItemsTheSame(
        old: ReceiptEntry,
        new: ReceiptEntry
    ) = old.id == new.id

    override fun areContentsTheSame(
        old: ReceiptEntry,
        new: ReceiptEntry
    ) = old == new
}

class ReceiptEntryViewHolder(val rv: DashboardReceiptAdapter, override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(
        item: ReceiptEntry,
        isOpen: Boolean,
        onClick: (ReceiptEntry) -> Unit
    ) {
        owner.text = item.owner
        address.text = item.address
        price.text = item.price
        date.text = convertLongToTime(item.date)
        containerView.setOnClickListener {
            onClick(item)
        }
    }

    companion object {
        fun create(rv: DashboardReceiptAdapter, parent: ViewGroup) = ReceiptEntryViewHolder(
            rv,
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_entry_receipt,
                parent,
                false
            )
        )
    }
}

fun convertLongToTime(time: Long): String {
    val date = Date(time)
    val format = SimpleDateFormat("yyyy.MM.dd HH:mm")
    return format.format(date)
}