package pl.codepunk.receiptvault.ui.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.databinding.LayoutCategoriesItemBinding
import pl.codepunk.receiptvault.databinding.LayoutResultsItemBinding
import pl.codepunk.receiptvault.ui.activities.EditResultActivity
import pl.codepunk.receiptvault.ui.viewmodel.ProcessedText
import java.lang.IllegalStateException

class ResultsAdapter(val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val data: MutableList<Row> = mutableListOf()

    fun setData(results: List<ProcessedText>) {
        data.clear()

        for (result in results) {
            data.add(LineRow(result))

            val sortedCategories = result.categories.toList().sortedBy { a -> a.second }.asReversed()

            for (category in sortedCategories) {
                data.add(ScoreRow(category.first, category.second.toString()))
            }
        }

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        1 -> ResultHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.layout_results_item,
                parent,
                false
            )
        )
        2 -> CategoryHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.layout_categories_item,
                parent,
                false
            )
        )
        else -> throw IllegalStateException()
    }

    override fun getItemViewType(position: Int): Int = when (data[position]) {
        is LineRow -> 1
        is ScoreRow -> 2
        else -> throw IllegalStateException()
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = when (holder) {
        is ResultHolder -> holder.bind(data[position] as LineRow)
        is CategoryHolder -> holder.bind(data[position] as ScoreRow)
        else -> throw IllegalStateException()
    }
}

class ResultHolder(private val binding: LayoutResultsItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: LineRow) {
        binding.item = item
        binding.executePendingBindings()
    }
}

class CategoryHolder(private val binding: LayoutCategoriesItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ScoreRow) {
        binding.item = item
        binding.executePendingBindings()
    }
}
abstract class Row {
    public fun fixItemCategory(view: View, text: ProcessedText) {
        view.context.startActivity(Intent(view.context, EditResultActivity::class.java).apply {
            putExtra("TEXT_TO_FIX", text)
        })
    }

    public fun findFirstBestSection(text: ProcessedText): String {
        return "HEADER = ${text.sections["HEADER"]}" + "\n" + "BODY = ${text.sections["BODY"]}" + "\n" + "TAIL = ${text.sections["TAIL"]}"
    }
}
data class LineRow(val value: ProcessedText) : Row()
data class ScoreRow(val key: String, val value: String) : Row()