package pl.codepunk.receiptvault.ui.fragments

import android.graphics.Rect
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Rational
import android.util.Size
import android.view.*
import androidx.camera.core.*
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.layout_camera.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.consume
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.toast
import pl.codepunk.receiptvault.ui.viewmodel.CameraViewModel
import javax.inject.Inject
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.permissions.PermissionsHelper
import pl.codepunk.receiptvault.utils.*
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraAccessException
import android.content.Context.CAMERA_SERVICE
import android.hardware.camera2.CameraManager
import androidx.camera.core.CameraX
import androidx.camera.core.CameraInfoUnavailableException
import android.annotation.SuppressLint
import androidx.lifecycle.Lifecycle
import java.lang.Math.round


private const val permissionRequestCode = 10001

@ExperimentalCoroutinesApi
class CameraFragment : ScopedFragment(), LifecycleOwner {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var permissionsHelper: PermissionsHelper

    private lateinit var viewModel: CameraViewModel
    private lateinit var imageCapture: ImageCapture
    private lateinit var preview: Preview
    private lateinit var cameraManager: CameraManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CameraViewModel::class.java)
        return inflater.inflate(R.layout.layout_camera, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cameraView.post {
            setupUI()
        }

        launch {
            viewModel.imageChannel.openSubscription().consume {
                findNavController().navigate(
                    CameraFragmentDirections.actionCameraFragmentToProcessReceiptFragment(
                        receive()
                    )
                )
            }
        }
    }

    override fun onStart() {
        super.onStart()

        cameraManager = activity?.getSystemService(CAMERA_SERVICE) as CameraManager

        if (!permissionsHelper.isPermissionGranted(android.Manifest.permission.CAMERA)) {
            requestPermissions(arrayOf(android.Manifest.permission.CAMERA), permissionRequestCode)
        }
    }

    override fun onResume() {
        super.onResume()
        if (permissionsHelper.isPermissionGranted(android.Manifest.permission.CAMERA)) {
            setupCamera()
        }
    }

    override fun onPause() {
        super.onPause()
        CameraX.unbindAll()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            permissionRequestCode -> {
                if (permissionsHelper.isPermissionGranted(android.Manifest.permission.CAMERA)) {
                    setupCamera()
                } else {
                    toast("Camera permission required")
                    activity?.finish()
                }
            }
            else -> {
            }
        }
    }

    private fun setupUI() {
        cameraControlView.setOnFocusPointSelectedListener { focus, metering ->

            val sensorSize = getActiveCamera()?.let { getSensorSize(it) } ?: Rect()
            val rescaledFocus = rescaleViewRectToSensorRect(focus, sensorSize)
            val rescaledMetering = rescaleViewRectToSensorRect(metering, sensorSize)

            preview.focus(rescaledFocus, rescaledMetering, object : OnFocusListener {
                override fun onFocusUnableToLock(afRect: Rect?) {
                    if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                        cameraControlView.enableTakePictureMode()
                    }
                }

                override fun onFocusTimedOut(afRect: Rect?) {
                    if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                        cameraControlView.enableTakePictureMode()
                    }
                }

                override fun onFocusLocked(afRect: Rect?) {
                    if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                        cameraControlView.enableTakePictureMode()
                    }
                }
            })
        }

        cameraControlView.setOnTakePictureSelectedListener {
            imageCapture.takePicture(object : ImageCapture.OnImageCapturedListener() {
                override fun onCaptureSuccess(image: ImageProxy, rotationDegrees: Int) {
                    image.planes.firstOrNull()?.let { proxy ->
                        viewModel.onImageCaptured(
                            ByteArray(proxy.buffer.remaining()).apply { proxy.buffer.get(this) },
                            rotationDegrees,
                            image.width,
                            image.height
                        )
                    }

                    super.onCaptureSuccess(image, rotationDegrees)
                }

                override fun onError(
                    useCaseError: ImageCapture.UseCaseError,
                    message: String,
                    cause: Throwable?
                ) {
                    viewModel.onImageCaptureError()
                    super.onError(useCaseError, message, cause)
                }
            })
        }
    }

    private fun setupCamera() = cameraView.post {
        CameraX.unbindAll()

        //val metrics = DisplayMetrics().also { parent.display.getRealMetrics(it) }
        val screenSize = Size(view!!.width, view!!.height)
        val screenAspectRatio = Rational(view!!.width, view!!.height)

        val viewFinderConfig = PreviewConfig.Builder().apply {
            setLensFacing(CameraX.LensFacing.BACK)
            setTargetResolution(screenSize)
            setTargetAspectRatio(screenAspectRatio)
            setTargetRotation(cameraView.display.rotation)
        }.build()

        val imageCaptureConfig = ImageCaptureConfig.Builder().apply {
            setLensFacing(CameraX.LensFacing.BACK)
            setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
            setTargetAspectRatio(screenAspectRatio)
            setTargetRotation(cameraView.display.rotation)
        }.build()

        preview = AutoFitPreviewBuilder.build(viewFinderConfig, cameraView)
        imageCapture = ImageCapture(imageCaptureConfig)

        CameraX.bindToLifecycle(this, preview, imageCapture)
    }

    private fun rescaleViewRectToSensorRect(view: Rect, sensor: Rect): Rect {
        // Scale width and height.
        val newWidth = round(view.width() * sensor.width() / 2000f)
        val newHeight = round(view.height() * sensor.height() / 2000f)

        // Scale top/left corner.
        val halfViewDimension = 2000 / 2
        val leftOffset = round((view.left + halfViewDimension) * sensor.width() / 2000f) + sensor.left
        val topOffset = round((view.top + halfViewDimension) * sensor.height() / 2000f) + sensor.top

        // Now, produce the scaled rect.
        val scaled = Rect()
        scaled.left = leftOffset
        scaled.top = topOffset
        scaled.right = scaled.left + newWidth
        scaled.bottom = scaled.top + newHeight
        return scaled
    }

    @Throws(CameraAccessException::class)
    private fun getSensorSize(cameraId: String): Rect? =
        cameraManager.getCameraCharacteristics(cameraId).get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE)

    @SuppressLint("RestrictedApi")
    @Throws(CameraInfoUnavailableException::class)
    fun getActiveCamera(): String? = CameraX.getCameraWithLensFacing(CameraX.LensFacing.BACK)
}