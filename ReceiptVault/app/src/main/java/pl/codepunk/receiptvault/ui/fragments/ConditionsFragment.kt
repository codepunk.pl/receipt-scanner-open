package pl.codepunk.receiptvault.ui.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.layout_conditions.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.act

import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.databinding.LayoutConditionsBinding
import pl.codepunk.receiptvault.databinding.LayoutDisclaimerBinding
import pl.codepunk.receiptvault.ui.activities.ContentActivity
import pl.codepunk.receiptvault.ui.viewmodel.ConditionsViewModel
import pl.codepunk.receiptvault.utils.ScopedFragment
import pl.codepunk.ui.setOnLinkClick
import javax.inject.Inject

class ConditionsFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ConditionsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ConditionsViewModel::class.java)

        launch {
            viewModel.navigationChannel.consumeEach { screen ->
                when (screen) {
                    0 -> findNavController().navigate(R.id.action_conditionsFragment_to_initFragment)
                    1 -> activity?.finish()
                }
            }
        }

        viewModel.onResume()

        val dataBinding: LayoutConditionsBinding =
            DataBindingUtil.inflate(inflater, R.layout.layout_conditions, container, false)
        dataBinding.vm = viewModel
        dataBinding.lifecycleOwner = this

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        text.setOnLinkClick { index ->
            context?.let {
                when (index) {
                    1 -> startActivity(ContentActivity.makeTermsAndConditions(it))
                    0 -> startActivity(ContentActivity.makePrivacyPolicy(it))
                }
            }
        }
    }
}