package pl.codepunk.receiptvault.ui.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.layout_dashboard.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.ui.adapters.DashboardReceiptAdapter
import pl.codepunk.receiptvault.ui.viewmodel.DashboardViewModel
import pl.codepunk.receiptvault.utils.ScopedFragment
import javax.inject.Inject

class DashboardFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_dashboard, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(DashboardViewModel::class.java)

        receiptsList.adapter = DashboardReceiptAdapter {receipt ->
            activity?.let {
                Navigation.findNavController(it, R.id.nav_host_fragment).navigate(HomeFragmentDirections.actionHomeFragmentToViewReceiptFragment(receipt))
            }
        }

        receiptsList.layoutManager = LinearLayoutManager(context)
        receiptsList.isNestedScrollingEnabled = false

        launch {
            viewModel.receipts.consumeEach { list ->
                receiptsList.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
                receiptsList.scheduleLayoutAnimation()

                (receiptsList.adapter as DashboardReceiptAdapter).submitList(list.sortedByDescending { it.date })
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.fetchReceipts()
    }
}
