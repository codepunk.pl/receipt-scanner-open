package pl.codepunk.receiptvault.ui.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.layout_add_receipt.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.databinding.LayoutDisclaimerBinding
import pl.codepunk.receiptvault.databinding.LayoutFirstAccessBinding
import pl.codepunk.receiptvault.preferences.PreferencesHelper
import pl.codepunk.receiptvault.ui.viewmodel.DisclaimerViewModel
import pl.codepunk.receiptvault.ui.viewmodel.FirstAccessViewModel
import pl.codepunk.receiptvault.utils.ScopedFragment
import pl.codepunk.receiptvault.utils.closeKeyboard
import javax.inject.Inject

class DisclaimerFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: DisclaimerViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DisclaimerViewModel::class.java)

        launch {
            viewModel.navigationChannel.consumeEach { screen ->
                when (screen) {
                    0 -> findNavController().navigate(R.id.action_disclaimerFragment_to_cameraFragment)
                    1 -> mainContainer.visibility = View.VISIBLE
                }
            }
        }

        viewModel.onResume()

        val dataBinding: LayoutDisclaimerBinding =
            DataBindingUtil.inflate(inflater, R.layout.layout_disclaimer, container, false)
        dataBinding.vm = viewModel
        dataBinding.lifecycleOwner = this

        return dataBinding.root
    }
}