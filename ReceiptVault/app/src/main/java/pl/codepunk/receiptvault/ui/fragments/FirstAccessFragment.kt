package pl.codepunk.receiptvault.ui.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.databinding.LayoutFirstAccessBinding
import pl.codepunk.receiptvault.ui.viewmodel.FirstAccessViewModel
import pl.codepunk.receiptvault.utils.ScopedFragment
import pl.codepunk.receiptvault.utils.closeKeyboard
import javax.inject.Inject

class FirstAccessFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: FirstAccessViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(FirstAccessViewModel::class.java)

        val dataBinding: LayoutFirstAccessBinding =
            DataBindingUtil.inflate(inflater, R.layout.layout_first_access, container, false)
        dataBinding.vm = viewModel
        dataBinding.lifecycleOwner = this

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        launch {
            viewModel.navigationChannel.consumeEach {screen ->
                when(screen) {
                    1 -> findNavController().navigate(R.id.action_firstAccessFragment_to_homeFragment)
                }
            }
        }
    }
}