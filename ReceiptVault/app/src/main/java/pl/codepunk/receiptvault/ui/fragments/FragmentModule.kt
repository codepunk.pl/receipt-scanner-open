package pl.codepunk.receiptvault.ui.fragments

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun historyFragment(): HistoryFragment

    @ContributesAndroidInjector
    abstract fun dashboardFragment(): DashboardFragment

    @ContributesAndroidInjector
    abstract fun cameraFragment(): CameraFragment

    @ContributesAndroidInjector
    abstract fun processReceiptFragment(): ProcessReceiptFragment

    @ContributesAndroidInjector
    abstract fun accessFragment(): AccessFragment

    @ContributesAndroidInjector
    abstract fun firstAccessFragment(): FirstAccessFragment

    @ContributesAndroidInjector
    abstract fun resetAccessFragment(): ResetAccessFragment

    @ContributesAndroidInjector
    abstract fun initFragment(): InitFragment

    @ContributesAndroidInjector
    abstract fun homeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun settingsFragment(): SettingsFragment

    @ContributesAndroidInjector
    abstract fun viewReceiptFragment(): ViewReceiptFragment

    @ContributesAndroidInjector
    abstract fun viewDisclaimerFramgnet(): DisclaimerFragment

    @ContributesAndroidInjector
    abstract fun viewConditionsFramgnet(): ConditionsFragment
}