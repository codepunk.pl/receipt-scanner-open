package pl.codepunk.receiptvault.ui.fragments

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.layout_dashboard.*
import kotlinx.android.synthetic.main.layout_home.*
import kotlinx.coroutines.async

import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.ui.activities.AddReceiptActivity
import pl.codepunk.receiptvault.ui.viewmodel.HomeViewModel
import pl.codepunk.receiptvault.ui.viewmodel.InitViewModel
import pl.codepunk.receiptvault.utils.ScopedFragment
import javax.inject.Inject

class HomeFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        addReceiptBtn.setOnClickListener {
            startActivity(Intent(context, AddReceiptActivity::class.java))
        }

        homeBtn.setOnClickListener {
            view?.findViewById<View>(R.id.nestedNavHostFragment)?.let {
                Navigation.findNavController(it).navigate(R.id.action_global_dashboardFragment)
            }
        }

        settingsBtn.setOnClickListener {
            view?.findViewById<View>(R.id.nestedNavHostFragment)?.let {
                Navigation.findNavController(it).navigate(R.id.action_global_settingsFragment)
            }
        }
    }
}
