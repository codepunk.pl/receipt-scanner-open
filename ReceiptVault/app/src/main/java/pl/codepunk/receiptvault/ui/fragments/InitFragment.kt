package pl.codepunk.receiptvault.ui.fragments

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.async
import kotlinx.coroutines.delay

import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.ui.activities.AddReceiptActivity
import pl.codepunk.receiptvault.ui.viewmodel.InitViewModel
import pl.codepunk.receiptvault.utils.ScopedFragment
import javax.inject.Inject

class InitFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: InitViewModel

    private var canNavigateToCamera: Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_init, container, false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode != 10001) return

        canNavigateToCamera = false

        async {
            when {
                viewModel.areConditionsAckAsync().await() -> {
                    findNavController().navigate(R.id.action_initFragment_to_conditionsFragment)
                }
                viewModel.isSessionActiveAsync().await() -> {
                    findNavController().navigate(R.id.action_initFragment_to_homeFragment)
                }
                viewModel.areCredentialsPresentAsync().await() -> {
                    findNavController().navigate(R.id.action_initFragment_to_accessFragment)
                }
                else -> {
                    findNavController().navigate(R.id.action_initFragment_to_firstAccessFragment)
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(InitViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()

        async {
            when {
                viewModel.areConditionsAckAsync().await() -> {
                    findNavController().navigate(R.id.action_initFragment_to_conditionsFragment)
                }
                viewModel.isSessionActiveAsync().await() -> {
                    findNavController().navigate(R.id.action_initFragment_to_homeFragment)
                }
                viewModel.areCredentialsPresentAsync().await() -> {
                    if (canNavigateToCamera) startActivityForResult(Intent(context, AddReceiptActivity::class.java), 10001)
                }
                else -> {
                    findNavController().navigate(R.id.action_initFragment_to_firstAccessFragment)
                }
            }
        }
    }
}
