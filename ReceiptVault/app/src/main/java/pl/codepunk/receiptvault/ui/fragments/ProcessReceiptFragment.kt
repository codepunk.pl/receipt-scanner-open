package pl.codepunk.receiptvault.ui.fragments

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.ui.viewmodel.ProcessReceiptViewModel
import javax.inject.Inject
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.layout_dashboard.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import pl.codepunk.receiptvault.databinding.LayoutProcessReceiptBinding
import pl.codepunk.receiptvault.ui.adapters.DashboardReceiptAdapter
import pl.codepunk.receiptvault.utils.ScopedFragment
import pl.codepunk.receiptvault.utils.ValueHolder
import pl.codepunk.receiptvault.utils.hideStatusUI
import pl.codepunk.receiptvault.utils.showStatusUI

class ProcessReceiptFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ProcessReceiptViewModel
    private val safeArgs: ProcessReceiptFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProcessReceiptViewModel::class.java)

        val dataBinding: LayoutProcessReceiptBinding =
            DataBindingUtil.inflate(inflater, R.layout.layout_process_receipt, container, false)
        dataBinding.vm = viewModel
        dataBinding.editTextHolder = ValueHolder<String>()
        dataBinding.editTagHolder = ValueHolder<Int>()
        dataBinding.companyNameEditTextHolder = ValueHolder<String>()
        dataBinding.companyAddressEditTextHolder = ValueHolder<String>()
        dataBinding.receiptSumEditTextHolder = ValueHolder<String>()
        dataBinding.receiptDateEditTextHolder = ValueHolder<String>()
        dataBinding.receiptPaymentByCardHolder = ValueHolder<Boolean>()
        dataBinding.lifecycleOwner = this

        viewModel.onProcessStart(safeArgs.cachedImageId)

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        launch {
            viewModel.navigationChannel.consumeEach { screen ->
                when (screen) {
                    0 -> activity?.finish()
                }
            }
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { _, keyCode, _ ->
            if(keyCode == KeyEvent.KEYCODE_BACK ) {
                return@setOnKeyListener viewModel.onBackPress()
            }

            return@setOnKeyListener false
        }
    }
}