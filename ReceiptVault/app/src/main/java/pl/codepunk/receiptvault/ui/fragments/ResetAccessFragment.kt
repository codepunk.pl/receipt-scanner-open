package pl.codepunk.receiptvault.ui.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.databinding.LayoutResetAccessBinding
import pl.codepunk.receiptvault.ui.viewmodel.ResetAccessViewModel
import pl.codepunk.receiptvault.utils.ScopedFragment
import javax.inject.Inject

class ResetAccessFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ResetAccessViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ResetAccessViewModel::class.java)

        val dataBinding: LayoutResetAccessBinding =
            DataBindingUtil.inflate(inflater, R.layout.layout_reset_access, container, false)
        dataBinding.vm = viewModel
        dataBinding.lifecycleOwner = this

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        launch {
            viewModel.navigationChannel.consumeEach {screen ->
                when(screen) {
                    1 -> findNavController().navigate(R.id.action_resetAccessFragment_to_homeFragment)
                }
            }
        }
    }
}
