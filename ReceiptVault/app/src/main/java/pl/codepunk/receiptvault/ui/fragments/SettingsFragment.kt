package pl.codepunk.receiptvault.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.layout_settings.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import pl.codepunk.receiptvault.BuildConfig
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.databinding.LayoutSettingsBinding
import pl.codepunk.receiptvault.preferences.PreferencesHelper
import pl.codepunk.receiptvault.ui.activities.ContentActivity
import pl.codepunk.receiptvault.ui.viewmodel.SettingsViewModel
import pl.codepunk.receiptvault.utils.ScopedFragment
import pl.codepunk.ui.launch
import pl.codepunk.ui.mailto
import pl.codepunk.ui.playStore
import javax.inject.Inject

class SettingsFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SettingsViewModel

    @Inject
    lateinit var preferencesHelper: PreferencesHelper // TODO move to MVVM

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SettingsViewModel::class.java)

        val dataBinding: LayoutSettingsBinding =
            DataBindingUtil.inflate(inflater, R.layout.layout_settings, container, false)
        dataBinding.vm = viewModel
        dataBinding.lifecycleOwner = this

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        darkModeBtn.setOnClickListener {
            AppCompatDelegate.setDefaultNightMode(if (darkModeBtn.isChecked) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
            preferencesHelper.nightMode = AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES
        }

        launch {
            viewModel.navigationChannel.consumeEach { screen ->
                context?.let {it ->
                    when (screen) {
                        1 -> it.launch(ContentActivity.makeAcknowledgements(it))
                        2 -> it.launch(ContentActivity.makePrivacyPolicy(it))
                        3 -> it.launch(ContentActivity.makeTermsAndConditions(it))
                        4 -> it.launch(ContentActivity.makeHelp(it))
                        5 -> it.mailto(getString(R.string.app_name), BuildConfig.VERSION_NAME)
                        6 -> it.playStore(BuildConfig.APPLICATION_ID)
                        else -> {}
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        darkModeBtn.isChecked =
            AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES
    }
}