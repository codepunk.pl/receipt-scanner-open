package pl.codepunk.receiptvault.ui.fragments

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.databinding.LayoutViewBinding
import pl.codepunk.receiptvault.ui.viewmodel.ViewReceiptViewModel
import pl.codepunk.receiptvault.utils.ScopedFragment
import pl.codepunk.receiptvault.utils.ValueHolder
import javax.inject.Inject

class ViewReceiptFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ViewReceiptViewModel
    private val safeArgs: ViewReceiptFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let { act ->
            act.setTheme(R.style.AppTheme_Dark)

            act.window?.let { window ->
                window.statusBarColor = resources.getColor(R.color.background, null)
                window.decorView.systemUiVisibility = window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                window.setBackgroundDrawable(ColorDrawable(window.statusBarColor))
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        when (AppCompatDelegate.getDefaultNightMode()) {
            AppCompatDelegate.MODE_NIGHT_YES -> activity?.let { act ->
                act.setTheme(R.style.AppTheme_Dark)

                act.window?.let { window ->
                    window.statusBarColor = resources.getColor(R.color.background, null)
                    window.decorView.systemUiVisibility = window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                    window.setBackgroundDrawable(ColorDrawable(window.statusBarColor))
                }
            }
            else -> activity?.let {act ->
                act.setTheme(R.style.AppTheme_Light)

                act.window?.let { window ->
                    window.statusBarColor = resources.getColor(R.color.light_background, null)
                    window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    window.setBackgroundDrawable(ColorDrawable(window.statusBarColor))
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ViewReceiptViewModel::class.java)

        val dataBinding: LayoutViewBinding =
            DataBindingUtil.inflate(inflater, R.layout.layout_view, container, false)
        dataBinding.vm = viewModel
        dataBinding.editTextHolder = ValueHolder<String>()
        dataBinding.editTagHolder = ValueHolder<Int>()
        dataBinding.companyNameEditTextHolder = ValueHolder<String>()
        dataBinding.companyAddressEditTextHolder = ValueHolder<String>()
        dataBinding.receiptSumEditTextHolder = ValueHolder<String>()
        dataBinding.receiptDateEditTextHolder = ValueHolder<String>()
        dataBinding.receiptPaymentByCardHolder = ValueHolder<Boolean>()
        dataBinding.lifecycleOwner = this

        viewModel.onProcessStartAsync(safeArgs.receipt)

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        launch {
            viewModel.navigationChannel.consumeEach { screen ->
                when (screen) {
                    0 -> {
                        activity?.let {
                            Navigation.findNavController(it, R.id.nav_host_fragment).navigate(R.id.action_viewReceiptFragment_to_homeFragment2)
                        }
                    }
                }
            }
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { _, keyCode, _ ->
            if(keyCode == KeyEvent.KEYCODE_BACK ) {
                return@setOnKeyListener viewModel.onBackPress()
            }

            return@setOnKeyListener false
        }
    }
}