package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import pl.codepunk.receiptvault.session.Password
import pl.codepunk.receiptvault.session.SessionManager
import pl.codepunk.receiptvault.utils.ErrorCode
import javax.inject.Inject

@ExperimentalCoroutinesApi
class AccessViewModel @Inject constructor(private val sessionManager: SessionManager) : ViewModel() {
    var cachedPassword: Password = ""

    val navigationChannel = Channel<Int>()
    val errorCode = MutableLiveData<ErrorCode>()

    fun setPassword(password: Password) = viewModelScope.launch {
        cachedPassword = password
        errorCode.postValue(0)
    }

    fun login() = viewModelScope.launch {
        sessionManager.activate(cachedPassword)
        if (sessionManager.isActive()) {
            errorCode.postValue(0)
            navigationChannel.offer(1)
        } else {
            errorCode.postValue(1)
        }
    }

    fun forgotPassword() = viewModelScope.launch {
        navigationChannel.offer(2)
    }

    override fun onCleared() {
        super.onCleared()
        navigationChannel.cancel()
    }
}