package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import pl.codepunk.receiptvault.processing.image.ImageCache
import javax.inject.Inject

@ExperimentalCoroutinesApi
class AddReceiptViewModel @Inject constructor(private val imageCache: ImageCache) : ViewModel() {
    override fun onCleared() {
        super.onCleared()
        imageCache.clearCache()
    }
}