package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.launch
import pl.codepunk.receiptvault.processing.image.ImageCache
import pl.codepunk.receiptvault.processing.image.ImageCachedToken
import javax.inject.Inject

@ExperimentalCoroutinesApi
class CameraViewModel @Inject constructor(private val imageCache: ImageCache) : ViewModel() {
    val imageChannel = BroadcastChannel<ImageCachedToken>(1)

    override fun onCleared() {
        super.onCleared()
        imageChannel.cancel()
    }

    fun onImageCaptured(image: ByteArray, rotationDegrees: Int, width: Int, height: Int) = viewModelScope.launch {
        imageChannel.offer(imageCache.putIntoCache(image, rotationDegrees, width, height))
    }

    fun onImageCaptureError() = viewModelScope.launch {

    }
}
