package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.preferences.PreferencesHelper
import javax.inject.Inject

@ExperimentalCoroutinesApi
class ConditionsViewModel @Inject constructor(private val preferencesHelper: PreferencesHelper) : ViewModel() {
    val navigationChannel = ConflatedBroadcastChannel<Int>()

    fun onResume() {
        if (preferencesHelper.conditions) {
            navigationChannel.offer(0)
        }
    }

    fun onAck() {
        preferencesHelper.conditions = true
        navigationChannel.offer(0)
    }

    fun onDec() {
        preferencesHelper.conditions = false
        navigationChannel.offer(1)
    }

    override fun onCleared() {
        super.onCleared()
        navigationChannel.cancel()
    }
}