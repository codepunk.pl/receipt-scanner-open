package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import pl.codepunk.receiptvault.repository.ReceiptRepository
import pl.codepunk.receiptvault.repository.model.ReceiptEntry
import javax.inject.Inject

@ExperimentalCoroutinesApi
class DashboardViewModel @Inject constructor(private val receiptRepository: ReceiptRepository) :
    ViewModel() {
    val receipts = Channel<List<ReceiptEntry>>()

    override fun onCleared() {
        super.onCleared()
        receipts.cancel()
    }

    fun fetchReceipts() = viewModelScope.launch(Dispatchers.IO) {
        receiptRepository.database?.run {
            receiptDao().getAll().let { _receipts ->
                receipts.send(_receipts)
            }
        }
    }
}
