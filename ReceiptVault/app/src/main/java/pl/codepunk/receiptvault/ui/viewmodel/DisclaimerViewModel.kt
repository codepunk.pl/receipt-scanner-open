package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.preferences.PreferencesHelper
import javax.inject.Inject

@ExperimentalCoroutinesApi
class DisclaimerViewModel @Inject constructor(private val preferencesHelper: PreferencesHelper) : ViewModel() {
    val navigationChannel = ConflatedBroadcastChannel<Int>()

    fun onResume() {
        if (preferencesHelper.disclaimer) {
            navigationChannel.offer(0)
        } else {
            navigationChannel.offer(1)
        }
    }

    fun onAck() {
        preferencesHelper.disclaimer = true
        navigationChannel.offer(0)
    }

    override fun onCleared() {
        super.onCleared()
        navigationChannel.cancel()
    }
}