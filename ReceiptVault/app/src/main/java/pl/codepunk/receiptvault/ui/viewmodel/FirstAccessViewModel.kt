package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.launch
import pl.codepunk.receiptvault.session.Password
import pl.codepunk.receiptvault.session.SessionManager
import pl.codepunk.receiptvault.utils.ErrorCode
import javax.inject.Inject

@ExperimentalCoroutinesApi
class FirstAccessViewModel @Inject constructor(private val sessionManager: SessionManager) : ViewModel() {

    var cachedRePassword: Password = ""
    var cachedNewPassword: Password = ""

    val navigationChannel = Channel<Int>()
    val errorCode = MutableLiveData<ErrorCode>()
    val reErrorCode = MutableLiveData<ErrorCode>()

    fun setPassword(password: Password) = viewModelScope.launch {
        cachedNewPassword = password

        errorCode.postValue(0)
        reErrorCode.postValue(0)
    }

    fun setRePassword(password: Password) = viewModelScope.launch {
        cachedRePassword = password

        reErrorCode.postValue(0)
    }

    fun savePassword() = viewModelScope.launch {
        if (cachedNewPassword == cachedRePassword && !cachedNewPassword.isBlank() && cachedNewPassword.length > 4) {
            sessionManager.setPassword(cachedNewPassword)
            navigationChannel.offer(1)
        }

        if (cachedNewPassword != cachedRePassword) {
            reErrorCode.postValue(2)
        }

        if (cachedNewPassword.isBlank() || cachedNewPassword.length <= 4) {
            errorCode.postValue(3)
        }
    }

    override fun onCleared() {
        super.onCleared()
        navigationChannel.cancel()
    }
}