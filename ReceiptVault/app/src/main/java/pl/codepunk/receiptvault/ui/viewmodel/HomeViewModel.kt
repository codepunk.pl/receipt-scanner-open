package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import pl.codepunk.receiptvault.session.SessionManager
import javax.inject.Inject

@ExperimentalCoroutinesApi
class HomeViewModel @Inject constructor() : ViewModel() {

}