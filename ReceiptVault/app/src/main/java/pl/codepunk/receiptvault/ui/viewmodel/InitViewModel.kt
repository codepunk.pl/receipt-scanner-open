package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import pl.codepunk.receiptvault.preferences.PreferencesHelper
import pl.codepunk.receiptvault.session.SessionManager
import javax.inject.Inject

@ExperimentalCoroutinesApi
class InitViewModel @Inject constructor(private val sessionManager: SessionManager, private val preferencesHelper: PreferencesHelper) : ViewModel() {

    fun isSessionActiveAsync() = viewModelScope.async {
        sessionManager.isActive()
    }

    fun areCredentialsPresentAsync() = viewModelScope.async {
        sessionManager.isPasswordPresent()
    }

    fun areConditionsAckAsync() =  viewModelScope.async {
        !preferencesHelper.conditions
    }
}