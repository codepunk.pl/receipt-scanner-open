package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.launch
import pl.codepunk.receiptvault.session.SessionManager
import javax.inject.Inject

@ExperimentalCoroutinesApi
class MainViewModel @Inject constructor() : ViewModel() {
    /*val navigationChannel = ConflatedBroadcastChannel<Int>()

    fun onScreenSelected(index: Int) = viewModelScope.launch {
        navigationChannel.offer(index)
    }

    override fun onCleared() {
        super.onCleared()
        navigationChannel.cancel()
    }*/
}