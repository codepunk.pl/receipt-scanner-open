package pl.codepunk.receiptvault.ui.viewmodel

import android.graphics.*
import android.os.Parcelable
import android.util.Log
import android.util.SparseArray
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.text.Line
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import pl.codepunk.receiptvault.processing.ai.*
import pl.codepunk.receiptvault.processing.image.CachedImageData
import pl.codepunk.receiptvault.processing.image.ImageCache
import pl.codepunk.receiptvault.processing.parser.*
import pl.codepunk.receiptvault.repository.ReceiptRepository
import pl.codepunk.receiptvault.repository.archive.ImageRepository
import pl.codepunk.receiptvault.ui.views.ResultSelection
import javax.inject.Inject
import javax.inject.Provider
import kotlin.Comparator
import android.graphics.Bitmap
import java.io.ByteArrayOutputStream

/**
 * TODO
 */
class ProcessReceiptViewModel @Inject constructor(
    private val imageCache: ImageCache,
    private val aiReceipt: AiReceipt,
    private val aiSection: AiSection,
    private val aiCardOrMoney: AiCardOrMoney,
    private val aiCurrency: AiCurrency,
    private val aiTime: AiTime,
    private val aiReceiptNoSection: AiReceiptSectionLess,
    private val imageRepository: ImageRepository,
    private val receiptRepository: ReceiptRepository,
    private val textRecognizerBuilderProvider: Provider<TextRecognizer.Builder>
) : ViewModel() {
    val navigationChannel = ConflatedBroadcastChannel<Int>()

    val liveImage = MutableLiveData<CachedImageData>()
    val liveZones = MutableLiveData<ResultSelection>()
    val liveResults = MutableLiveData<List<ResultSelection>>()
    val liveEdit = MutableLiveData<EditableText>()
    val liveSummary = MutableLiveData<Boolean>()

    val liveFilterSelected = MutableLiveData<Int>()

    val liveSummaryAddress = MutableLiveData<String>()
    val liveSummaryCompany = MutableLiveData<String>()
    val liveSummaryDate = MutableLiveData<String>()
    val liveSummaryValue = MutableLiveData<String>()
    val liveSummaryPaymentCard = MutableLiveData<Boolean>()

    val liveProcessing = MutableLiveData<Boolean>()
    val liveSummaryAddressError = MutableLiveData<String>()
    val liveSummaryCompanyError = MutableLiveData<String>()
    val liveSummaryDateError = MutableLiveData<String>()
    val liveSummaryValueError = MutableLiveData<String>()

    private val liveUnmappedText = MutableLiveData<List<EditableText>>()
    private val liveMappedText = MutableLiveData<Map<String, MutableList<EditableText>>>()

    private var compressedImage: ByteArray? = null

    /**
     * TODO
     */
    fun onZoneSelected(zone: Rect) {
        val value = liveMappedText.value?.values?.flatten()?.find {
            it.originText.line.boundingBox.contains(zone)
        } ?: liveUnmappedText.value?.find { it.originText.line.boundingBox.contains(zone) }
        value?.let {
            liveEdit.value = it
            liveSummary.value = null
        }
    }

    /**
     * TODO
     */
    fun onUpdateLine(line: EditableText, text: String?, tag: Int?) {
        liveEdit.value = null
        line.editText = text
        line.editTag = when (tag) {
            1 -> "FIRMA"
            2 -> "ADRES"
            3 -> "DATA"
            4 -> "INFO-PLATNOSC"
            5 -> "SUMA"
            else -> null
        }

        val _value = liveMappedText.value
        _value?.values?.forEach { it.removeAll { originLine -> originLine == line } }
        _value?.get(line.editTag ?: line.originTag)?.add(line)
        liveMappedText.value = _value

        liveResults.value = _value?.mapValues { value ->
            ResultSelection(
                value.value.map { it.originText.line.boundingBox },
                getSectionColor(value.key)
            )
        }?.values?.toList()

        updateSummary()

        liveFilterSelected.value = null
    }

    /**
     * TODO
     */
    fun onDeleteLine(line: EditableText) {
        liveEdit.value = null

        val _value = liveMappedText.value
        _value?.values?.forEach { it.removeAll { originLine -> originLine == line } }
        liveMappedText.value = _value

        liveResults.value = _value?.mapValues { value ->
            ResultSelection(
                value.value.map { it.originText.line.boundingBox },
                getSectionColor(value.key)
            )
        }?.values?.toList()

        updateSummary()

        liveFilterSelected.value = null
    }

    /**
     * TODO
     */
    fun onFilterSelected(type: Int) {
        when (type) {
            1 -> filterType("FIRMA")
            2 -> filterType("ADRES")
            3 -> filterType("DATA")
            4 -> filterType("INFO-PLATNOSC")
            5 -> filterType("SUMA")
            else -> {
            }
        }

        liveFilterSelected.value = type
    }

    /**
     * TODO
     */
    fun onSummary() {
        liveEdit.value = null
        liveSummary.value = true
    }

    /**
     * TODO
     */
    private fun filterType(tag: String) = viewModelScope.launch(Dispatchers.IO) {
        val result =
            liveMappedText.value?.get(tag)?.map { it.originText.line.boundingBox } ?: listOf()
        withContext(Dispatchers.Main) {
            liveResults.value = listOf(ResultSelection(result, getSectionColor(tag)))
        }
    }

    /**
     * TODO
     */
    fun onBackPress(): Boolean {
        if (liveEdit.value != null) {
            liveEdit.value = null
            return true
        }

        if (liveSummary.value == true) {
            liveSummary.value = null
            return true
        }

        return false
    }

    /**
     * TODO
     */
    fun onProcessStart(cachedImageId: String) = viewModelScope.launch {
        liveProcessing.value = true

        try {
            val imageData = imageCache.getAndClearFromCache(cachedImageId)
            liveImage.value = imageData
            withContext(Dispatchers.IO) {
                imageData?.let { data ->
                    // Prepare image to store
                    val compressedImageJob = compressImageAsync(data.image, data.rotationDegrees)
                    compressedImageJob.start()

                    // Process image
                    val extractedText = extractTextAsync(data).await()

                    val processedText = processTextAsync(extractedText).await()
                    val processedTextBackup = processTextBackupAsync(extractedText).await()

                    val processedReceipt = extractReceipt(processedText)
                    val processedReceiptBackup = extractReceipt(processedTextBackup)

                    // Prepare map
                    val tagMap = mutableMapOf<String, MutableList<EditableText>>()

                    tagMap["ADRES"] = tagMap["ADRES"] ?: mutableListOf()
                    processedReceipt.adresy.forEach {
                        tagMap["ADRES"]?.add(EditableText(originText = it, originTag = "ADRES"))
                    }

                   /* processedReceiptBackup.adresy.forEach {
                        tagMap["ADRES"]?.add(EditableText(originText = it, originTag = "ADRES"))
                    }*/

                    tagMap["FIRMA"] = tagMap["FIRMA"] ?: mutableListOf()
                    processedReceipt.firmy.forEach {
                        tagMap["FIRMA"]?.add(EditableText(originText = it, originTag = "FIRMA"))
                    }

                    /*processedReceiptBackup.firmy.forEach {
                        tagMap["FIRMA"]?.add(EditableText(originText = it, originTag = "FIRMA"))
                    }*/

                    tagMap["DATA"] = tagMap["DATA"] ?: mutableListOf()
                    processedReceipt.daty.forEach {
                        tagMap["DATA"]?.add(EditableText(originText = it, originTag = "DATA"))
                    }

                    processedReceiptBackup.daty.forEach {
                        tagMap["DATA"]?.add(EditableText(originText = it, originTag = "DATA"))
                    }

                    tagMap["SUMA"] = tagMap["SUMA"] ?: mutableListOf()
                    processedReceipt.sumy.forEach {
                        tagMap["SUMA"]?.add(EditableText(originText = it, originTag = "SUMA"))
                    }

                    processedReceiptBackup.sumy.forEach {
                        tagMap["SUMA"]?.takeIf { processedReceipt.sumy.isEmpty() }?.add(EditableText(originText = it, originTag = "SUMA"))
                    }

                    tagMap["INFO-PLATNOSC"] = tagMap["INFO-PLATNOSC"] ?: mutableListOf()
                    processedReceipt.platnosci.forEach {
                        tagMap["INFO-PLATNOSC"]?.add(
                            EditableText(
                                originText = it,
                                originTag = "INFO-PLATNOSC"
                            )
                        )
                    }

                    processedReceiptBackup.platnosci.forEach {
                        tagMap["INFO-PLATNOSC"]?.add(
                            EditableText(
                                originText = it,
                                originTag = "INFO-PLATNOSC"
                            )
                        )
                    }

                    // Update zones
                    withContext(Dispatchers.Main) {
                        liveZones.value =
                            ResultSelection(
                                extractedText.flatMap { it.components }.map { it.boundingBox },
                                Color.TRANSPARENT
                            )
                    }

                    // Update sections
                    withContext(Dispatchers.Main) {
                        liveResults.value = tagMap.mapValues { value ->
                            ResultSelection(
                                value.value.map { it.originText.line.boundingBox },
                                getSectionColor(value.key)
                            )
                        }.values.toList()
                    }

                    // Store results
                    withContext(Dispatchers.Main) {
                        liveMappedText.value = tagMap
                        liveUnmappedText.value =
                            extractedText.flatMap { it.components }.map { text ->
                                EditableText(
                                    originText = ProcessedText(
                                        line = ProcessedLine(
                                            text.value,
                                            text.boundingBox
                                        )
                                    ),
                                    originTag = null
                                )
                            }
                    }

                    withContext(Dispatchers.Main) {
                        liveFilterSelected.value = null
                        updateSummary()
                    }

                    compressedImage = compressedImageJob.await()
                }
            }

        } catch (t: Throwable) {
            // TODO
        }

        liveProcessing.value = false
    }

    /**
     * TODO
     */
    private fun updateSummary() = viewModelScope.launch(Dispatchers.IO) {
        val cardOrMoney =
            liveMappedText.value?.get("INFO-PLATNOSC")?.lastOrNull()?.let { editableText ->
                aiCardOrMoney.predictCategory(
                    editableText.editText ?: editableText.originText.line.value
                ).filter { entry -> entry.value > 0.8f }["KARTA"] != null
            } ?: false

        val currencyGroups = liveUnmappedText.value?.map { editableText ->
            editableText.originText.line.value.split(" ").mapNotNull {
                aiCurrency.predictCategory(it)
                    .filter { entry -> entry.value > 0.8f && entry.key != "NAC" }.keys.firstOrNull()
            }
        }?.flatten()?.groupBy { it }

        var currencyType: String? = ""
        var currencyTypeCount = 0

        currencyGroups?.forEach {
            if (it.value.size >= currencyTypeCount) {
                currencyTypeCount = it.value.size
                currencyType = it.value.firstOrNull()
            }
        }

        val data = liveMappedText.value?.get("DATA")?.toList()
            ?.map { editableText ->
                editableText.editText ?: editableText.originText.line.value
            }
            ?.merge()

        val dayFormat = DateTimeFormat.forPattern("dd/MM/yyyy")
        val timeFormat = DateTimeFormat.forPattern("HH:mm")
        val currentDate = DateTime.now()

        val dateDay =
            data?.split(" ")?.filter { aiTime.predictCategory(it).filter { entry -> entry.value > 0.8f }["DATA"] != null }?.firstOrNull()
                ?: dayFormat.print(currentDate)
        val dateTime =
            data?.split(" ")?.filter { aiTime.predictCategory(it).filter { entry -> entry.value > 0.8f }["GODZINA"] != null }?.firstOrNull()
                ?: timeFormat.print(currentDate)

        val suma = liveMappedText.value?.get("SUMA")?.toList()
            ?.map { editableText ->
                editableText.editText ?: editableText.originText.line.value
            }
            ?.merge()

        withContext(Dispatchers.Main) {
            liveSummaryAddress.value = liveMappedText.value?.get("ADRES")?.toList()
                ?.map { editableText ->
                    editableText.editText ?: editableText.originText.line.value
                }
                ?.map {text ->
                    text.split(" ").map {
                        it.toLowerCase().capitalize()
                    }.merge()
                }
                ?.merge()

            liveSummaryCompany.value = liveMappedText.value?.get("FIRMA")?.toList()
                ?.map { editableText ->
                    editableText.editText ?: editableText.originText.line.value
                }
                ?.map {text ->
                    text.split(" ").map {
                        it.toLowerCase().capitalize()
                    }.merge()
                }
                ?.merge()

            liveSummaryDate.value = "$dateDay $dateTime"

            liveSummaryValue.value = "$currencyType $suma"

            liveSummaryPaymentCard.value = cardOrMoney
        }
    }

    /**
     * TODO
     */
    private fun getSectionColor(key: String): Int = when (key) {
        "ADRES" -> Color.GREEN
        "DATA" -> Color.CYAN
        "FIRMA" -> Color.BLUE
        "INFO-PLATNOSC" -> Color.YELLOW
        "INFO-SUMA" -> Color.TRANSPARENT
        "SUMA" -> Color.MAGENTA
        else -> Color.TRANSPARENT
    }

    /**
     * TODO
     */
    private fun extractTextAsync(image: CachedImageData) = viewModelScope.async {
        val bitmap = BitmapFactory.decodeByteArray(image.image, 0, image.image.size)

        try {
            val frame = Frame.Builder()
                .setBitmap(bitmap)
                .setRotation(image.rotationDegrees / 90)
                .build()

            return@async textRecognizerBuilderProvider.get()
                .build()
                .detect(frame)
                .flattenTextBlock()

        } finally {
            bitmap.recycle()
        }
    }

    /**
     * TODO
     */
    private fun processTextAsync(input: List<Line>) = viewModelScope.async {
        val tempResults: MutableList<Line> = input.toMutableList()

        // Sort
        tempResults.sortWith(Comparator { p1, p2 ->
            when {
                p1.boundingBox.centerY() > p2.boundingBox.centerY() -> 1
                p1.boundingBox.centerY() == p2.boundingBox.centerY() -> 0
                else -> -1
            }
        })

        // Process
        return@async tempResults.mapIndexed { index, text ->
            val sectionsScore = aiSection.predictSection(index, tempResults.size)
            val array = sectionsScore.values.toFloatArray()
            val categoriesScore = aiReceipt.predictCategory(array, text.value)

            ProcessedText(
                index,
                tempResults.size,
                ProcessedLine(text.value, text.boundingBox),
                sectionsScore,
                categoriesScore
            )
        }.toList()
    }

    /**
     * TODO
     */
    private fun processTextBackupAsync(input: List<Line>) = viewModelScope.async {
        val tempResults: MutableList<Line> = input.toMutableList()

        // Sort
        tempResults.sortWith(Comparator { p1, p2 ->
            when {
                p1.boundingBox.centerY() > p2.boundingBox.centerY() -> 1
                p1.boundingBox.centerY() == p2.boundingBox.centerY() -> 0
                else -> -1
            }
        })

        // Process
        return@async tempResults.mapIndexed { index, text ->
            val categoriesScore = aiReceiptNoSection.predictCategory(text.value)

            ProcessedText(
                index,
                tempResults.size,
                ProcessedLine(text.value, text.boundingBox),
                mapOf(),
                categoriesScore
            )
        }.toList()
    }

    /**
     * TODO
     */
    fun dismissSummary() {
        liveSummary.value = false
    }

    /**
     * TODO
     */
    fun storeReceiptAndQuit(
        companyName: String?,
        companyAddress: String?,
        receiptDate: String?,
        receiptTotalSum: String?,
        receiptPaymentCardType: Boolean
    ) = viewModelScope.async {
        liveProcessing.value = true

        liveSummaryAddressError.value = null
        liveSummaryCompanyError.value = null
        liveSummaryDateError.value = null
        liveSummaryValueError.value = null

        try {
            withContext(Dispatchers.IO) {
                val date = receiptDate?.split(" ")
                    ?.filter { aiTime.predictCategory(it).filter { entry -> entry.value > 0.8f }["DATA"] != null }
                    ?.firstOrNull()?.let { interpretDate(it) }

                val time = receiptDate?.split(" ")
                    ?.filter { aiTime.predictCategory(it).filter { entry -> entry.value > 0.8f }["GODZINA"] != null }
                    ?.firstOrNull()?.let { interpretTime(it) }

                val dateTime = date?.toDateTime(time)

                val currency = receiptTotalSum?.split(" ")
                    ?.filter { aiCurrency.predictCategory(it).filter { entry -> entry.value > 0.8f }["NAC"] == null }
                    ?.firstOrNull()

                val sum = currency?.let {
                    receiptTotalSum.replace(it, "").replace(" ", "")
                }

                val currencySum = "$currency $sum"
                val validReceiptDate = dateTime?.millis
                val validReceiptSum = parseMoney(currencySum)
                val validCompanyName = parseName(companyName)
                val validCompanyAddress = parseName(companyAddress)

                if (validReceiptDate == null) {
                    // TODO notify error
                    withContext(Dispatchers.Main) {
                        liveSummaryDateError.value = "Invalid date (e.g. 22/12/2019 09:12)"
                    }
                }

                if (validReceiptSum == null) {
                    withContext(Dispatchers.Main) {
                        liveSummaryValueError.value = "Invalid price (e.g. GBP 10.0)"
                    }
                }

                if (validCompanyName == null) {
                    withContext(Dispatchers.Main) {
                        liveSummaryCompanyError.value = "Invalid business name"
                    }
                }

                if (validCompanyAddress == null) {
                    withContext(Dispatchers.Main) {
                        liveSummaryAddressError.value = "Invalid address"
                    }
                }

                if (validReceiptDate == null || validReceiptSum == null || validCompanyName == null || validCompanyAddress == null) {
                    return@withContext
                }

                liveImage.value?.let { imageHolder ->
                    val imageId = imageRepository.store(compressedImage ?: imageHolder.image)
                    val result = receiptRepository.storeReceipt(
                        validCompanyName,
                        validCompanyAddress,
                        validReceiptDate,
                        validReceiptSum,
                        if (receiptPaymentCardType) 1 else 0,
                        imageId
                    )

                    if (!result) {
                        // TODO notify error
                    } else {
                        navigationChannel.offer(0)
                    }
                } ?: run {
                    // TODO notify error
                }
            }
        } catch (t: Throwable) {
            // TODO
        }

        liveProcessing.value = false
    }

    private fun compressImageAsync(image: ByteArray, rotationDegrees: Int) =
        viewModelScope.async(Dispatchers.IO) {
            skipCrash {
                val byteArrayOutputStream = ByteArrayOutputStream()
                val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size)

                val fixedBitmap = fixBitmap(bitmap, 0.4f, rotationDegrees)
                fixedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream)

                val byteArray = byteArrayOutputStream.toByteArray()

                skipCrash { bitmap.recycle() }
                skipCrash { fixedBitmap.recycle() }
                skipCrash { byteArrayOutputStream.close() }

                byteArray
            }
        }

    override fun onCleared() {
        super.onCleared()
        navigationChannel.cancel()
    }
}

/**
 * TODO
 */
fun fixBitmap(
    bitmap: Bitmap,
    scaleRatio: Float,
    rotationDegrees: Int
): Bitmap {
    val matrix = Matrix()
    matrix.postScale(scaleRatio, scaleRatio, 0f, 0f)
    matrix.postRotate(rotationDegrees.toFloat())

    return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
}

/**
 * TODO
 */
private fun SparseArray<TextBlock>.flattenTextBlock(): List<Line> {
    val tempResults = mutableListOf<Line>()
    // Flatten tree
    for (i in 0 until size()) {
        val item = valueAt(i)
        val components = item.components
        for (j in components.indices) {
            val text = components[j]
            if (text is Line) {
                tempResults.add(text)
            }
        }
    }

    return tempResults
}

/**
 * TODO
 */
@Parcelize
data class ProcessedText(
    val index: Int = -1,
    val size: Int = -1,
    val line: ProcessedLine,
    val sections: Score = mapOf(),
    val categories: Score = mapOf()
) : Parcelable

@Parcelize
data class ProcessedLine(
    val value: String,
    val boundingBox: Rect
) : Parcelable

/**
 * TODO
 */
@Parcelize
data class EditableText(
    val originText: ProcessedText,
    val originTag: String?,
    var editText: String? = null,
    var editTag: String? = null
) : Parcelable
