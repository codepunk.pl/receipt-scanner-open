package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import pl.codepunk.receiptvault.repository.ReceiptRepository
import pl.codepunk.receiptvault.repository.archive.ImageRepository
import pl.codepunk.receiptvault.session.Password
import pl.codepunk.receiptvault.session.SessionManager
import pl.codepunk.receiptvault.utils.ErrorCode
import javax.inject.Inject

@ExperimentalCoroutinesApi
class ResetAccessViewModel @Inject constructor(
    private val sessionManager: SessionManager,
    private val receiptRepository: ReceiptRepository,
    private val imageRepository: ImageRepository
) : ViewModel() {
    var cachedRePassword: Password = ""
    var cachedNewPassword: Password = ""

    val navigationChannel = Channel<Int>()
    val errorCode = MutableLiveData<ErrorCode>()
    val reErrorCode = MutableLiveData<ErrorCode>()

    fun setPassword(password: Password) = viewModelScope.launch {
        cachedNewPassword = password

        errorCode.postValue(0)
        reErrorCode.postValue(0)
    }

    fun setRePassword(password: Password) = viewModelScope.launch {
        cachedRePassword = password

        reErrorCode.postValue(0)
    }

    fun savePassword() = viewModelScope.launch {
        if (cachedNewPassword == cachedRePassword && !cachedNewPassword.isBlank() && cachedNewPassword.length > 4) {
            sessionManager.setPassword(cachedNewPassword)

            withContext(Dispatchers.IO) {
                receiptRepository.reInitRepository()
                imageRepository.reInitRepository()
            }

            navigationChannel.offer(1)
        }

        if (cachedNewPassword != cachedRePassword) {
            reErrorCode.postValue(2)
        }

        if (cachedNewPassword.isBlank() || cachedNewPassword.length <= 4) {
            errorCode.postValue(3)
        }
    }

    override fun onCleared() {
        super.onCleared()
        navigationChannel.cancel()
    }
}