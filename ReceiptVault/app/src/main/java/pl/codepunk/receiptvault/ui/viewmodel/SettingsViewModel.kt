package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import pl.codepunk.receiptvault.BuildConfig
import pl.codepunk.receiptvault.repository.ReceiptRepository
import pl.codepunk.receiptvault.repository.archive.ImageRepository
import pl.codepunk.ui.humanReadableByteCount
import javax.inject.Inject

class SettingsViewModel @Inject constructor(imageRepository: ImageRepository, receiptRepository: ReceiptRepository) : ViewModel() {
    val receiptsInfoDesc = MutableLiveData<String>()
    val databaseInfoDesc = MutableLiveData<String>()
    val version = MutableLiveData<String>()
    val navigationChannel = Channel<Int>()

    init {
        viewModelScope.launch {
            val imgRepoSize = imageRepository.size()
            val imgRepoItemCount = imageRepository.count()

            withContext(Dispatchers.Main) {
                receiptsInfoDesc.value = "Receipts count: $imgRepoItemCount size: ${humanReadableByteCount(imgRepoSize)}"
                databaseInfoDesc.value = "App database size: ${humanReadableByteCount(receiptRepository.size())}"
                version.value = "${BuildConfig.VERSION_NAME}.${BuildConfig.BUILD_TYPE}"
            }
        }
    }

    fun ack() {
        navigationChannel.offer(1)
    }

    fun privacy() {
        navigationChannel.offer(2)
    }

    fun terms() {
        navigationChannel.offer(3)
    }

    fun help() {
        navigationChannel.offer(4)
    }

    fun report() {
        navigationChannel.offer(5)
    }

    fun playStore() {
        navigationChannel.offer(6)
    }

    override fun onCleared() {
        super.onCleared()
        navigationChannel.cancel()
    }
}