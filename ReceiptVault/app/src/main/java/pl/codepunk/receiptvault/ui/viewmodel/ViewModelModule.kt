package pl.codepunk.receiptvault.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pl.codepunk.receiptvault.utils.ViewModelFactory
import pl.codepunk.receiptvault.utils.ViewModelKey

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HistoryViewModel::class)
    internal abstract fun bindHistoryViewModel(viewModel: HistoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    internal abstract fun bindDashboardViewModel(viewModel: DashboardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddReceiptViewModel::class)
    internal abstract fun bindAddReceiptViewModel(viewModel: AddReceiptViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CameraViewModel::class)
    internal abstract fun bindCameraViewModel(viewModel: CameraViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProcessReceiptViewModel::class)
    internal abstract fun bindProcessReceiptViewModel(viewModel: ProcessReceiptViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccessViewModel::class)
    internal abstract fun bindAccessViewModel(viewModel: AccessViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ResetAccessViewModel::class)
    internal abstract fun bindResetAccessViewModel(viewModel: ResetAccessViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FirstAccessViewModel::class)
    internal abstract fun bindFirstAccessViewModel(viewModel: FirstAccessViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InitViewModel::class)
    internal abstract fun bindInitViewModel(viewModel: InitViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    internal abstract fun bindSettingsViewModel(viewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ViewReceiptViewModel::class)
    internal abstract fun bindViewReceiptViewModel(viewModel: ViewReceiptViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DisclaimerViewModel::class)
    internal abstract fun bindDisclaimerViewModel(viewModel: DisclaimerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConditionsViewModel::class)
    internal abstract fun binConditionsViewModel(viewModel: ConditionsViewModel): ViewModel
}