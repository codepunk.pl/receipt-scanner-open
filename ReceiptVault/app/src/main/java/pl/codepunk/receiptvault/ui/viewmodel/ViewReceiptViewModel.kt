package pl.codepunk.receiptvault.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.withContext
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import pl.codepunk.receiptvault.processing.ai.AiCurrency
import pl.codepunk.receiptvault.processing.ai.AiTime
import pl.codepunk.receiptvault.processing.image.CachedImageData
import pl.codepunk.receiptvault.processing.parser.interpretDate
import pl.codepunk.receiptvault.processing.parser.interpretTime
import pl.codepunk.receiptvault.processing.parser.parseMoney
import pl.codepunk.receiptvault.processing.parser.parseName
import pl.codepunk.receiptvault.repository.ReceiptRepository
import pl.codepunk.receiptvault.repository.archive.ImageRepository
import pl.codepunk.receiptvault.repository.model.ReceiptEntry
import javax.inject.Inject

class ViewReceiptViewModel @Inject constructor(
    private val receiptRepository: ReceiptRepository,
    private val imageRepository: ImageRepository,
    private val aiCurrency: AiCurrency,
    private val aiTime: AiTime
) : ViewModel() {
    private var cachedReceipt: ReceiptEntry? = null

    val navigationChannel = ConflatedBroadcastChannel<Int>()
    val liveSummary = MutableLiveData<Boolean>()
    val liveProcessing = MutableLiveData<Boolean>()

    val liveSummaryAddress = MutableLiveData<String>()
    val liveSummaryCompany = MutableLiveData<String>()
    val liveSummaryDate = MutableLiveData<String>()
    val liveSummaryValue = MutableLiveData<String>()
    val liveSummaryPaymentCard = MutableLiveData<Boolean>()

    val liveSummaryAddressError = MutableLiveData<String>()
    val liveSummaryCompanyError = MutableLiveData<String>()
    val liveSummaryDateError = MutableLiveData<String>()
    val liveSummaryValueError = MutableLiveData<String>()

    val liveImage = MutableLiveData<CachedImageData>()

    fun onProcessStartAsync(receipt: ReceiptEntry) = viewModelScope.async {
        liveProcessing.value = true

        withContext(Dispatchers.IO) {
            try {
                val file = imageRepository.retrieve(receipt.file)
                val reci = receiptRepository.database?.receiptDao()?.get(receipt.id)

                withContext(Dispatchers.Main) {
                    if (file != null && reci != null) {
                        cachedReceipt = reci
                        liveImage.value = CachedImageData(file, 0, 0, 0)

                        liveSummaryAddress.value = reci.address
                        liveSummaryCompany.value = reci.owner
                        liveSummaryDate.value =
                            DateTime(reci.date).toString(DateTimeFormat.forPattern("dd/MM/yyyy HH:mm"))
                        liveSummaryValue.value = reci.price
                        liveSummaryPaymentCard.value = reci.paymentType == 1

                    } else {
                        navigationChannel.offer(0)
                    }
                }
            } catch (error: Throwable) {
                navigationChannel.offer(0)
            }
        }

        liveProcessing.value = false
    }

    /**
     * TODO
     */
    fun storeReceiptAndQuit(
        companyName: String?,
        companyAddress: String?,
        receiptDate: String?,
        receiptTotalSum: String?,
        receiptPaymentCardType: Boolean
    ) = viewModelScope.async {
        liveProcessing.value = true
        liveSummaryAddressError.value = null
        liveSummaryCompanyError.value = null
        liveSummaryDateError.value = null
        liveSummaryValueError.value = null
        withContext(Dispatchers.IO) {

            try {
                val date = receiptDate?.split(" ")
                    ?.filter { aiTime.predictCategory(it).filter { entry -> entry.value > 0.8f }["DATA"] != null }
                    ?.firstOrNull()?.let { interpretDate(it) }

                val time = receiptDate?.split(" ")
                    ?.filter { aiTime.predictCategory(it).filter { entry -> entry.value > 0.8f }["GODZINA"] != null }
                    ?.firstOrNull()?.let { interpretTime(it) }

                val dateTime = date?.toDateTime(time)

                val currency = receiptTotalSum?.split(" ")
                    ?.filter { aiCurrency.predictCategory(it).filter { entry -> entry.value > 0.8f }["NAC"] == null }
                    ?.firstOrNull()

                val sum = currency?.let {
                    receiptTotalSum.replace(it, "").replace(" ", "")
                }

                val currencySum = "$currency $sum"
                val validReceiptDate = dateTime?.millis
                val validReceiptSum = parseMoney(currencySum)
                val validCompanyName = parseName(companyName)
                val validCompanyAddress = parseName(companyAddress)

                if (validReceiptDate == null) {
                    withContext(Dispatchers.Main) {
                        liveSummaryDateError.value = "Invalid date (e.g. 22/12/2019 09:12)"
                    }
                }

                if (validReceiptSum == null) {
                    withContext(Dispatchers.Main) {
                        liveSummaryValueError.value = "Invalid price (e.g. GBP 10.0)"
                    }
                }

                if (validCompanyName == null) {
                    withContext(Dispatchers.Main) {
                        liveSummaryCompanyError.value = "Invalid business name"
                    }
                }

                if (validCompanyAddress == null) {
                    withContext(Dispatchers.Main) {
                        liveSummaryAddressError.value = "Invalid address"
                    }
                }

                if (validReceiptDate == null || validReceiptSum == null || validCompanyName == null || validCompanyAddress == null) {
                    return@withContext
                }

                cachedReceipt?.let { cReceipt ->
                    val result = receiptRepository.updateReceipt(
                        validCompanyName,
                        validCompanyAddress,
                        validReceiptDate,
                        validReceiptSum,
                        if (receiptPaymentCardType) 1 else 0,
                        cReceipt.file,
                        cReceipt.id
                    )

                    if (!result) {
                        // TODO notify error
                    } else {
                        navigationChannel.offer(0)
                    }
                } ?: run {
                    // TODO notify error
                }
            } catch (t: Throwable) {
                // TODO
            }
        }

        liveProcessing.value = false
    }

    /**
     * TODO
     */
    fun delete() = viewModelScope.async {
        liveProcessing.value = true

        withContext(Dispatchers.IO) {
            cachedReceipt?.let {
                receiptRepository.database?.receiptDao()?.delete(it.id)
                imageRepository.remove(it.file)
            }

            navigationChannel.offer(0)
        }

        liveProcessing.value = false
    }

    /**
     * TODO
     */
    fun onEdit() {
        liveSummary.value = true
    }

    /**
     * TODO
     */
    fun onBackPress(): Boolean {
        if (liveSummary.value == true) {
            liveSummary.value = null
            return true
        }

        return false
    }

    /**
     * TODO
     */
    override fun onCleared() {
        super.onCleared()
        navigationChannel.cancel()
    }
}