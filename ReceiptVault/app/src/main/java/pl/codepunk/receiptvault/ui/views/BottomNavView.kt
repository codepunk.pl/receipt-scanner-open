package pl.codepunk.receiptvault.ui.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet

import com.google.android.material.bottomnavigation.BottomNavigationView

class BottomNavView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : BottomNavigationView(context, attrs, defStyle) {

    init {
        setBackgroundColor(Color.TRANSPARENT)
    }
}
