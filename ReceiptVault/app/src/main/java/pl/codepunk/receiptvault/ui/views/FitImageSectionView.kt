package pl.codepunk.receiptvault.ui.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.databinding.BindingAdapter
import pl.codepunk.receiptvault.processing.image.CachedImageData
import kotlin.math.abs

class FitImageSectionView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val scaleOff = 0.45f

    private var bmpImage: Bitmap? = null
    private var cachedImageArray: ByteArray? = null
    private var cachedImageRotation: Int = 0

    private var imageMatrix: Matrix = Matrix()
    private var focusArea = Rect()

    private var xImgScale = 1f
    private var yImgScale = 1f
    private var imgScale = 1f
    private var xImgTranslation = 0f
    private var yImgTranslation = 0f
    private var padFromTop = 0f
    private var padFromStart = 0f

    private val overlayPaint = Paint().apply {
        color = Color.BLACK
        style = Paint.Style.FILL
    }

    fun setImage(image: ByteArray, rotationDegrees: Int) {
        cachedImageArray = image
        cachedImageRotation = rotationDegrees

        val rotateMatrix = Matrix().apply {
            postRotate(rotationDegrees.toFloat())
        }

        bmpImage?.recycle()

        val srcBmp = BitmapFactory.decodeByteArray(image, 0, image.size)
        val rotatedBmp =
            Bitmap.createBitmap(srcBmp, 0, 0, srcBmp.width, srcBmp.height, rotateMatrix, true)
        srcBmp.recycle()

        bmpImage = rotatedBmp
        //focusArea = Rect(0, 0, rotatedBmp.width, rotatedBmp.height)

        recalcView()
    }

    private fun recalcView() {
        xImgScale = width.toFloat() / focusArea.width()
        yImgScale = height.toFloat() / focusArea.height()
        imgScale = abs((if (xImgScale <= yImgScale) xImgScale else yImgScale) - scaleOff)

        xImgTranslation = focusArea.left * -1f * imgScale
        yImgTranslation = focusArea.top * -1f * imgScale

        padFromTop = (height - (focusArea.height() * imgScale)) * 0.5f
        padFromStart = (width - (focusArea.width() * imgScale)) * 0.5f

        invalidate()
    }

    fun focusOnArea(area: Rect) {
        focusArea = area
        recalcView()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        recalcView()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        cachedImageArray?.let {
            setImage(it, cachedImageRotation)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        bmpImage?.recycle()
        bmpImage = null
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawRect(0f, 0f, width * 1f, height * 1f, overlayPaint)

        bmpImage?.let {
            imageMatrix.apply {
                reset()
                postScale(imgScale, imgScale)
                postTranslate(xImgTranslation + padFromStart, yImgTranslation + padFromTop)
            }

            canvas.drawBitmap(it, imageMatrix, null)
        }

        //canvas.drawRect(0f, 0f, width * 1f, padFromTop, overlayPaint)
        //canvas.drawRect(0f, height - padFromTop, width * 1f , height * 1f, overlayPaint)
    }
}

@BindingAdapter("app:rawSrc")
fun rawSrc(view: FitImageSectionView, array: CachedImageData?) =
    array?.let { view.setImage(it.image, it.rotationDegrees) }

@BindingAdapter("app:area")
fun area(view: FitImageSectionView, area: Rect?) = area?.let { view.focusOnArea(area) }
