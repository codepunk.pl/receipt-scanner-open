package pl.codepunk.receiptvault.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.animation.ValueAnimator
import android.animation.Animator
import android.view.animation.BounceInterpolator
import androidx.appcompat.widget.AppCompatDrawableManager
import pl.codepunk.receiptvault.R

class FocusOverlayView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val cameraDrawable =
        AppCompatDrawableManager.get().getDrawable(context, R.drawable.ic_camera).apply {
            setTint(resources.getColor(R.color.red, null))
            bounds = Rect(0, 0, intrinsicWidth, intrinsicHeight)
        }

    private lateinit var meteringRect: Rect
    private lateinit var focusingRect: Rect

    private var drawRect: Boolean = false
    private var drawCamera: Boolean = false

    private var outerRingAnimator: Animator? = null
    private var scaleValue = 1f

    private var cachedLastUpEvent: MotionEvent? = null

    private var focusPointListener: (Rect, Rect) -> Unit = { _, _ -> }
    private var picturePointListener: () -> Unit = { }

    private var strokeInnerPaint = Paint().apply {
        style = Paint.Style.FILL
        color = resources.getColor(R.color.red, null)
        strokeWidth = 6f
        isAntiAlias = true
    }

    private var strokeOuterPaint = Paint().apply {
        style = Paint.Style.STROKE
        color = resources.getColor(R.color.red, null)
        strokeWidth = 6f
        isAntiAlias = true
    }

    fun setOnFocusPointSelectedListener(function: (Rect, Rect) -> Unit) {
        focusPointListener = function
    }

    fun setOnTakePictureSelectedListener(function: () -> Unit) {
        picturePointListener = function
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (!drawRect) {
            return
        }

        canvas.drawCircle(
            meteringRect.exactCenterX(),
            meteringRect.exactCenterY(),
            (meteringRect.width() / 2f) * scaleValue,
            strokeOuterPaint
        )

        if (!drawCamera) {
            canvas.drawCircle(
                focusingRect.exactCenterX(),
                focusingRect.exactCenterY(),
                focusingRect.width() / 2f,
                strokeInnerPaint
            )
        } else {
            canvas.translate(
                meteringRect.exactCenterX() - cameraDrawable.intrinsicWidth / 2,
                meteringRect.exactCenterY() - cameraDrawable.intrinsicHeight / 2
            )

            canvas.scale(
                0.25f,
                0.25f,
                cameraDrawable.intrinsicWidth / 2f,
                cameraDrawable.intrinsicHeight / 2f
            )

            cameraDrawable.draw(canvas)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (MotionEvent.ACTION_DOWN == event.action) {
            if (drawCamera && meteringRect.contains(event.x.toInt(), event.y.toInt())) {
                picturePointListener()
                return false
            }

            return true
        }

        if (MotionEvent.ACTION_UP == event.action) {
            cachedLastUpEvent = event
            performClick()

            return true
        }

        return super.onTouchEvent(event)
    }

    override fun performClick(): Boolean {
        drawCamera = false
        recalculateFocusAndMeteringPos()

        if (area(focusingRect) > 0 && area(meteringRect) > 0) {
            focusPointListener(focusingRect, meteringRect)
        }

        return super.performClick()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        recalculateFocusAndMeteringPos()
    }

    fun enableTakePictureMode() {
        drawCamera = true
        recalculateFocusAndMeteringPos()
    }

    private fun recalculateFocusAndMeteringPos() {
        val x = cachedLastUpEvent?.x ?: x + width / 2f
        val y = cachedLastUpEvent?.y ?: y + height / 2f

        focusingRect = calculateTapArea(x, y, 0.1f)
        meteringRect = calculateTapArea(x, y, 0.65f)

        outerRingAnimator?.cancel()
        outerRingAnimator = ValueAnimator.ofFloat(1.5f, 1f).apply {
            addUpdateListener { animation ->
                drawRect = true
                scaleValue = animation.animatedValue as Float
                invalidate()
            }

            interpolator = BounceInterpolator()
            duration = 500
            start()
        }
    }

    private fun calculateTapArea(_x: Float, _y: Float, coefficient: Float) = Rect().apply {
        val areaSize = (300 * coefficient)
        left = (_x - areaSize / 2).toInt()
        top = (_y - areaSize / 2).toInt()
        right = (_x + areaSize / 2).toInt()
        bottom = (_y + areaSize / 2).toInt()
    }

    private fun area(rect: Rect): Int {
        return rect.width() * rect.height()
    }
}