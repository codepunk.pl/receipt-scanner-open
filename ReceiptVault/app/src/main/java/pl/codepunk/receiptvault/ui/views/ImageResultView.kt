package pl.codepunk.receiptvault.ui.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import androidx.core.graphics.withMatrix
import androidx.databinding.BindingAdapter
import pl.codepunk.receiptvault.processing.image.CachedImageData
import kotlin.math.abs

class ImageResultView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val targetZones: MutableList<Rect> = mutableListOf()
    private val selectionAlpha: Int = 125

    private val selectedTargetsPaints: MutableMap<ResultSelection, Paint> = mutableMapOf()
    private val selectedTargets: MutableList<ResultSelection> = mutableListOf()

    private var lastFocusX: Float = 1f
    private var lastFocusY: Float = 1f

    private var selectionListener: OnSelectionClickListener = object : OnSelectionClickListener {
        override fun onClick(rect: Rect) = Unit
    }

    fun setSelectionListener(listener: OnSelectionClickListener) {
        selectionListener = listener
    }

    private val scaleListener = object : ScaleGestureDetector.OnScaleGestureListener {
        override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
            lastFocusX = detector.focusX
            lastFocusY = detector.focusY
            return true
        }

        override fun onScaleEnd(detector: ScaleGestureDetector) {
        }

        override fun onScale(detector: ScaleGestureDetector): Boolean {
            val transformationMatrix = Matrix()
            val focusX = detector.focusX
            val focusY = detector.focusY

            transformationMatrix.postTranslate(-focusX, -focusY)
            transformationMatrix.postScale(detector.scaleFactor, detector.scaleFactor)

            val focusShiftX = focusX - lastFocusX
            val focusShiftY = focusY - lastFocusY
            transformationMatrix.postTranslate(focusX + focusShiftX, focusY + focusShiftY)
            imageMatrix.postConcat(transformationMatrix)
            selectionMatrix.postConcat(transformationMatrix)

            lastFocusX = focusX
            lastFocusY = focusY
            invalidate()
            return true
        }
    }

    private val gestureListener = object : GestureDetector.OnGestureListener {
        override fun onShowPress(p0: MotionEvent) {
        }

        override fun onSingleTapUp(tapEvent: MotionEvent): Boolean {
            val foundRect = targetZones.find {
                val testRect = RectF(
                    it.left.toFloat(),
                    it.top.toFloat(),
                    it.right.toFloat(),
                    it.bottom.toFloat()
                )
                selectionMatrix.mapRect(testRect)

                testRect.contains(
                    tapEvent.x,
                    tapEvent.y
                )
            }

            foundRect?.let {
                selectionListener.onClick(it)
            }

            return foundRect != null
        }

        override fun onDown(p0: MotionEvent): Boolean {
            return false
        }

        override fun onFling(p0: MotionEvent, p1: MotionEvent, p2: Float, p3: Float): Boolean {
            return false
        }

        override fun onScroll(
            downEvent: MotionEvent,
            currentEvent: MotionEvent,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            imageMatrix.postTranslate(-distanceX, -distanceY)
            selectionMatrix.postTranslate(-distanceX, -distanceY)
            invalidate()
            return true
        }

        override fun onLongPress(p0: MotionEvent) {

        }
    }

    private val scaleGesture = ScaleGestureDetector(context, scaleListener)
    private val gestures = GestureDetector(context, gestureListener)

    private var bmpImage: Bitmap? = null
    private var cachedImageArray: ByteArray? = null
    private var cachedImageRotation: Int = 0

    private var imageMatrix: Matrix = Matrix()
    private var selectionMatrix: Matrix = Matrix()

    private val overlayPaint = Paint().apply {
        color = Color.BLACK
        style = Paint.Style.FILL
    }

    fun setImage(image: ByteArray, rotationDegrees: Int) {
        cachedImageArray = image
        cachedImageRotation = rotationDegrees

        bmpImage?.recycle()
        bmpImage = BitmapFactory.decodeByteArray(image, 0, image.size)
        imageMatrix.postRotate(
            rotationDegrees.toFloat(),
            bmpImage!!.width * 0.5f,
            bmpImage!!.height * 0.5f
        )

        selectionMatrix.postRotate(-rotationDegrees.toFloat())
        selectionMatrix.postTranslate(0f, bmpImage!!.height * 1f)
        selectionMatrix.postRotate(
            rotationDegrees.toFloat(),
            bmpImage!!.width * 0.5f,
            bmpImage!!.height * 0.5f
        )

        var xImgScale = width.toFloat() / bmpImage!!.width
        var yImgScale = height.toFloat() / bmpImage!!.height

        if (rotationDegrees in 90..180 || rotationDegrees in 270..360) {
            xImgScale = width.toFloat() / bmpImage!!.height
            yImgScale = height.toFloat() / bmpImage!!.width
        }

        val imgScale = abs((if (xImgScale >= yImgScale) xImgScale else yImgScale))
        imageMatrix.postScale(imgScale, imgScale)
        selectionMatrix.postScale(imgScale, imgScale)

        val yImgTranslation = ((bmpImage!!.height * imgScale) * 0.5f - height * 0.5f) * -1
        val xImgTranslation = ((bmpImage!!.width * imgScale) * 0.5f - width * 0.5f) * -1
        imageMatrix.postTranslate(xImgTranslation, yImgTranslation)
        selectionMatrix.postTranslate(xImgTranslation, yImgTranslation)

        invalidate()
    }

    fun setZones(zones: ResultSelection) {
        targetZones.clear()
        targetZones.addAll(zones.selectionArray)
    }

    fun setSelection(targets: List<ResultSelection>) {
        selectedTargetsPaints.clear()
        targets.forEach { selection ->
            selectedTargetsPaints[selection] = Paint().apply {
                isAntiAlias = true
                xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)
                color = Color.argb(
                    selectionAlpha,
                    Color.red(selection.selectionColor),
                    Color.green(selection.selectionColor),
                    Color.blue(selection.selectionColor)
                )
            }
        }

        selectedTargets.clear()
        selectedTargets.addAll(targets)
        invalidate()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        cachedImageArray?.let {
            setImage(it, cachedImageRotation)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        bmpImage?.recycle()
        bmpImage = null
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)

        scaleGesture.onTouchEvent(event)
        gestures.onTouchEvent(event)

        return true
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawRect(0f, 0f, width * 1f, height * 1f, overlayPaint)

        bmpImage?.let {
            canvas.drawBitmap(it, imageMatrix, null)
        }

        canvas.withMatrix(selectionMatrix) {
            selectedTargets.forEach {
                selectedTargetsPaints[it]?.let { paint ->
                    it.selectionArray.forEach { box ->
                        canvas.drawRect(box, paint)
                    }
                }
            }
        }
    }
}

@BindingAdapter("app:rawSrc")
fun rawSrc(view: ImageResultView, array: CachedImageData?) =
    array?.let { view.setImage(it.image, it.rotationDegrees) }

@BindingAdapter("app:selectionSrc")
fun selectionSrc(view: ImageResultView, selection: List<ResultSelection>?) =
    selection?.let { view.setSelection(it) }

@BindingAdapter("app:zonesSrc")
fun zonesSrc(view: ImageResultView, selection: ResultSelection?) =
    selection?.let { view.setZones(it) }

@BindingAdapter("app:onSelectionClick")
fun onSelectionClick(view: ImageResultView, listener: OnSelectionClickListener) =
    view.setSelectionListener(listener)

interface OnSelectionClickListener {
    fun onClick(rect: Rect)
}

data class ResultSelection(val selectionArray: List<Rect>, val selectionColor: Int)