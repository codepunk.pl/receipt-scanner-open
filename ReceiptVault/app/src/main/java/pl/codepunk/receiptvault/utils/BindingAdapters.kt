package pl.codepunk.receiptvault.utils

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import pl.codepunk.receiptvault.R
import pl.codepunk.receiptvault.ui.adapters.ResultsAdapter
import pl.codepunk.receiptvault.ui.viewmodel.ProcessedText
import pl.codepunk.ui.CoreButton

typealias ErrorCode = Int

@BindingAdapter("app:adapterValues")
fun adapterValues(recyclerView: RecyclerView, values: List<ProcessedText>?) {
    if (recyclerView.adapter == null) {
        recyclerView.adapter = ResultsAdapter(recyclerView.context)
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    }

    val adapter: ResultsAdapter = recyclerView.adapter as ResultsAdapter
    adapter.setData(values ?: emptyList())
}

@BindingAdapter("app:errorCode")
fun errorCode(textView: TextView, value: ErrorCode) {
    textView.visibility = View.VISIBLE

    when (value) {
        1 -> textView.text = textView.resources.getString(R.string.wrong_password)
        2 -> textView.text = textView.resources.getString(R.string.wrong_re_password)
        3 -> textView.text = textView.resources.getString(R.string.wrong_struct_password)
        else -> textView.visibility = View.INVISIBLE
    }
}

@BindingAdapter("app:onError")
fun onError(textView: TextInputLayout, errorMessage: String?) {
    if (errorMessage != null) {
        textView.error = errorMessage
    } else {
        textView.error = null
    }
}

@BindingAdapter("android:onClick")
fun setFocusOnClick(view: View, listener: View.OnClickListener) {
    view.setOnClickListener {
        it?.closeKeyboard()
        listener.onClick(it)
    }
}

@BindingAdapter("onTag", "onLoad")
fun mapTagToInt(view: View, tag: String?, valueHolder: ValueHolder<Int>) {
    valueHolder.set(
        when (tag) {
            "FIRMA" -> 1
            "ADRES" -> 2
            "DATA" -> 3
            "INFO-PLATNOSC" -> 4
            "SUMA" -> 5
            else -> 0
        }
    )
}

@BindingAdapter("onTag", "onLoad")
fun mapTagToInt(view: View, tag: Boolean?, valueHolder: ValueHolder<Boolean>) {
    valueHolder.set(tag ?: false)
}

@BindingAdapter("coreButton:titleText")
fun setTitleText(coreButton: CoreButton, text: String?) {
    coreButton.titleVal = text
}