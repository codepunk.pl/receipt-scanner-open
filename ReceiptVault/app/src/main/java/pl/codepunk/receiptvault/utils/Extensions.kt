package pl.codepunk.receiptvault.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity

fun View.closeKeyboard() = context?.let {
    val mgr = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    mgr.hideSoftInputFromWindow(windowToken, 0)
}

fun Activity.hideStatusUI() {
    window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
}

fun Activity.showStatusUI() {
    window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
}