package pl.codepunk.receiptvault.utils

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext
import com.squareup.leakcanary.LeakCanary
import pl.codepunk.receiptvault.BuildConfig
import pl.codepunk.receiptvault.R

abstract class ScopedActivity : DaggerAppCompatActivity(), CoroutineScope {
    val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(BuildConfig.APPLICATION_NAME, "Caught $exception")
    }

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main + handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()

        val refWatcher = LeakCanary.installedRefWatcher()
        refWatcher.watch(this)
    }
}