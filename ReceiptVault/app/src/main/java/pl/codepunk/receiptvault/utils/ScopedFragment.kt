package pl.codepunk.receiptvault.utils

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.squareup.leakcanary.LeakCanary
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import pl.codepunk.receiptvault.BuildConfig
import pl.codepunk.receiptvault.R
import kotlin.coroutines.CoroutineContext
import android.view.View
import android.graphics.drawable.ColorDrawable

abstract class ScopedFragment : DaggerFragment(), CoroutineScope {
    val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(BuildConfig.APPLICATION_NAME, "Caught $exception")
    }

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main + handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()

        when (AppCompatDelegate.getDefaultNightMode()) {
            AppCompatDelegate.MODE_NIGHT_YES -> activity?.let {act ->
                act.setTheme(R.style.AppTheme_Dark)

                act.window?.let { window ->
                    window.statusBarColor = resources.getColor(R.color.background, null)
                    window.decorView.systemUiVisibility = window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                    window.setBackgroundDrawable(ColorDrawable(window.statusBarColor))
                }
            }
            else -> activity?.let {act ->
                act.setTheme(R.style.AppTheme_Light)

                act.window?.let { window ->
                    window.statusBarColor = resources.getColor(R.color.light_background, null)
                    window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    window.setBackgroundDrawable(ColorDrawable(window.statusBarColor))
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()

        val refWatcher = LeakCanary.installedRefWatcher()
        refWatcher.watch(this)
    }
}

