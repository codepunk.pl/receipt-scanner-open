package pl.codepunk.receiptvault.utils

import android.util.Log
import com.squareup.leakcanary.LeakCanary
import dagger.android.DaggerService
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import pl.codepunk.receiptvault.BuildConfig
import kotlin.coroutines.CoroutineContext

abstract class ScopedService: DaggerService(), CoroutineScope {
    val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(BuildConfig.APPLICATION_NAME, "Caught $exception")
    }

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main + handler

    override fun onCreate() {
        super.onCreate()
        job = Job()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()

        val refWatcher = LeakCanary.installedRefWatcher()
        refWatcher.watch(this)
    }
}