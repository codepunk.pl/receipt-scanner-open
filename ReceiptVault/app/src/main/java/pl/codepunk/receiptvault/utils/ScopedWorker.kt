package pl.codepunk.receiptvault.utils

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.squareup.leakcanary.LeakCanary
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import pl.codepunk.receiptvault.BuildConfig
import kotlin.coroutines.CoroutineContext

abstract class ScopedWorker(appContext: Context, params: WorkerParameters) : Worker(appContext, params),
    CoroutineScope {

    init {
        val refWatcher = LeakCanary.installedRefWatcher()
        refWatcher.watch(this)
    }

    val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(BuildConfig.APPLICATION_NAME, "Caught $exception")
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + handler

}