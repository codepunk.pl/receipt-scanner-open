package pl.codepunk.receiptvault.utils

import androidx.lifecycle.MutableLiveData

class ValueHolder<T>() {

    val live = MutableLiveData<T>()

    fun set(value: T?) {
        live.value = value
    }

    fun get(): T? = live.value

}