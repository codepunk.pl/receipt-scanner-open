package pl.codepunk.ui

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.layout_core_button.view.*
import android.graphics.Typeface
import android.view.MotionEvent
import android.widget.TextView


class CoreButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var titleVal
        get() = title.text?.toString()
        set(value) {
            title.text = value
        }

    var isChecked
        get() = rightSwitch.isChecked
        set(value) {
            rightSwitch.isChecked = value
        }

    private val rightIconEnabled: Boolean
    private val rightSwitchEnabled: Boolean

    init {
        inflate(context, R.layout.layout_core_button, this)

        val a = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CoreButton, 0, 0
        )

        try {
            val titleText = a.getString(R.styleable.CoreButton_titleText)
            val titleBold = a.getBoolean(R.styleable.CoreButton_titleBold, false)

            val labelText = a.getResourceId(R.styleable.CoreButton_labelText, 0)
            rightIconEnabled = a.getBoolean(R.styleable.CoreButton_rightIconEnabled, false)
            rightSwitchEnabled = a.getBoolean(R.styleable.CoreButton_rightSwitchEnabled, false)

            val rightIconType = a.getInt(R.styleable.CoreButton_rightIconType, 2)
            when (rightIconType) {
                0 -> rightIcon.setImageResource(R.drawable.ic_correct_symbol)
                1 -> rightIcon.setImageResource(R.drawable.ic_remove_symbol)
                else -> rightIcon.setImageResource(R.drawable.ic_right_chevron)
            }

            title.text = titleText

            if (titleBold) {
                title.typeface = Typeface.defaultFromStyle(Typeface.BOLD)
            }

            if (labelText == 0) {
                label.visibility = View.GONE
            } else {
                label.setText(labelText)
            }

            if (!rightIconEnabled) {
                rightIcon.visibility = View.GONE
            }

            if (!rightSwitchEnabled) {
                rightSwitch.visibility = View.GONE
            }

            rightSwitch.isClickable = false

        } finally {
            a.recycle()
        }

        setOnClickListener(null)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        super.setOnClickListener(toggleSwitchWrapper(l))
    }

    private fun toggleSwitchWrapper(wrappedListener: View.OnClickListener? = null): View.OnClickListener? =
        OnClickListener {
            rightSwitch.toggle()
            wrappedListener?.onClick(it)
        }

    fun lock() {
        isClickable = false

        progressBar.visibility = View.VISIBLE

        rightSwitch.visibility = View.GONE
        rightIcon.visibility = View.GONE
    }

    fun unlock() {
        isClickable = true

        progressBar.visibility = View.GONE

        if (!rightIconEnabled) {
            rightIcon.visibility = View.GONE
        } else {
            rightIcon.visibility = View.VISIBLE
        }

        if (!rightSwitchEnabled) {
            rightSwitch.visibility = View.GONE
        } else {
            rightSwitch.visibility = View.VISIBLE
        }
    }
}