package pl.codepunk.ui

import android.text.Spannable
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView

fun TextView.setOnLinkClick(callback: (index: Int) -> (Unit)) {
    movementMethod = LinkMovementMethod.getInstance()
    generateSequence { if (text.contains(linkRegex)) text else null }
        .map {
            linkRegex.find(it)?.let { result ->
                val link = result.groupValues[1]
                text = text.replaceRange(result.range, link)

                val start = result.range.start
                val end = start + link.length

                Pair(start, end)
            }
        }
        .take(2)
        .toList()
        .forEachIndexed { index, range ->
            if (range != null) {
                val spans = text as Spannable
                spans.setSpan(object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        widget.clearFocus()
                        callback(index)
                    }
                }, range.first, range.second, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
}

private val linkRegex = "\\[link\\](.*?)\\[\\/link\\]".toRegex()