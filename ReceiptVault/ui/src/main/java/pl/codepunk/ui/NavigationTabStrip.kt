package pl.codepunk.ui

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import android.widget.Scroller
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.viewpager.widget.ViewPager
import pl.codepunk.ui.R

// Max and min fraction
private const val MIN_FRACTION = 0.0f
private const val MAX_FRACTION = 1.0f

// NTS constants
private val HIGH_QUALITY_FLAGS = Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG

class NavigationTabStrip @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr), ViewPager.OnPageChangeListener {

    private var stripAnimator: ValueAnimator? = null

    private val stripPaint = Paint(HIGH_QUALITY_FLAGS).apply {
        style = Paint.Style.FILL
        color = context.getColor(R.color.colorActive)
    }

    private val stripHeight = context.resources.getDimension(R.dimen.stripSize)
    private val stripBounds = RectF()
    private val resizeInterpolator = ResizeInterpolator().apply {
        factor = 2.5f
    }

    private var stripLeft = 0f
    private var stripRight = 0f

    var pager: ViewPager? = null
        set(value) {
            field = value
            pager?.let {
                it.addOnPageChangeListener(this)
                resetScroller()
                postInvalidate()
            }
        }

    init {
        // Always draw
        setWillNotDraw(false)
        setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        orientation = HORIZONTAL

        addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            val itemIndex = pager?.currentItem ?: 0
            val item = getChildAt(itemIndex)
            val width = item.width.toFloat()
            val start = item.x
            val end = item.x + width

            updateIndicatorPosition(start, end, width, MIN_FRACTION)
        }
    }

    private var isMotionDownCurrentIndex = false

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
//                isMotionDownCurrentIndex = findTabIndex(event.x) == pager?.currentItem ?: 0
            }
            MotionEvent.ACTION_MOVE -> {
//                if (isMotionDownCurrentIndex) {
//                    pager?.setCurrentItem(findTabIndex(event.x), true)
//                }
            }
            MotionEvent.ACTION_UP -> {
                isMotionDownCurrentIndex = false
                val tabIndex = findTabIndex(event.x)
                if (tabIndex >= 0) {
                    playSoundEffect(0)
                }
                setTabIndex(tabIndex)
            }
        }

        return true
    }

    override fun onPageScrollStateChanged(state: Int) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            setTabIndex(pager?.currentItem ?: 0)
        }
    }

    override fun onPageScrolled(index: Int, positionOffset: Float, positionOffsetPixels: Int) {
        if (stripAnimator?.isRunning == true) return
        if (isMotionDownCurrentIndex) return

        val childOrigin = getChildAt(index)
        val childDestination = getChildAt(index + 1)

        childDestination?.let {

            val widthDelta = childDestination.width - childOrigin.width
            val scaledWidth = childOrigin.width + widthDelta * positionOffset

            updateIndicatorPosition(childOrigin.x, childDestination.x, scaledWidth, positionOffset)
        }
    }

    override fun onPageSelected(position: Int) {
    }

    private fun setTabIndex(index: Int) {
        if (index == -1) return
        if (index == pager?.currentItem) return

        val currentIndex = pager?.currentItem ?: 0
        val targetIndex = index

        val childOrigin = getChildAt(currentIndex)
        val childDestination = getChildAt(targetIndex)

        val widthDelta = childDestination.width - childOrigin.width

        stripAnimator?.cancel()
        stripAnimator = ValueAnimator().apply {
            interpolator = LinearInterpolator()
            duration = 300
            setFloatValues(MIN_FRACTION, MAX_FRACTION)
            addUpdateListener { animation ->
                val fraction = animation.animatedValue as Float

                val scaledWidth = childOrigin.width + widthDelta * fraction
                updateIndicatorPosition(
                    childOrigin.x,
                    childDestination.x,
                    scaledWidth,
                    fraction,
                    targetIndex < currentIndex
                )
            }

            resetScroller()
            start()
        }

        pager?.setCurrentItem(index, true)
    }

    private fun findTabIndex(x: Float): Int {
        for (i in 0 until childCount) {
            val childX = getChildAt(i).x
            val childWidth = getChildAt(i).width
            if (x >= childX && x < childX + childWidth) {
                return i
            }
        }

        return -1
    }

    private fun updateIndicatorPosition(start: Float, end: Float, width: Float, fraction: Float, isResizeIn: Boolean = false) {
        val a = resizeInterpolator.getResizeInterpolation(fraction, isResizeIn)
        val b = resizeInterpolator.getResizeInterpolation(fraction, !isResizeIn)

        // Set the strip left side coordinate
        stripLeft = start +
                (a * (end - start))
        // Set the strip right side coordinate
        stripRight = (start + width) +
                (b * (end - start))

        // Update NTS
        postInvalidate()
    }

    override fun onDraw(canvas: Canvas) {
        stripBounds.set(
            stripLeft,
            height.toFloat(),
            stripRight,
            height - stripHeight
        )

        canvas.drawRect(stripBounds, stripPaint)
    }

    private fun resetScroller() {
        if (pager == null) return
        try {
            val scrollerField = ViewPager::class.java.getDeclaredField("mScroller")
            scrollerField.isAccessible = true
            val scroller = ResizeViewPagerScroller(context)
            scrollerField.set(pager, scroller)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

private class ResizeViewPagerScroller(context: Context) : Scroller(context, AccelerateDecelerateInterpolator()) {

    override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) {
        super.startScroll(startX, startY, dx, dy, 300)
    }

    override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int) {
        super.startScroll(startX, startY, dx, dy, 300)
    }
}

private class ResizeInterpolator : Interpolator {

    // Spring factor
    var factor: Float = 0.toFloat()

    // Check whether side we move
    private var mResizeIn: Boolean = false

    override fun getInterpolation(input: Float): Float {
        return if (mResizeIn)
            (1.0 - Math.pow((1.0 - input), (2.0 * factor))).toFloat()
        else
            Math.pow(1.0 * input, (2.0 * factor)).toFloat()
    }

    fun getResizeInterpolation(input: Float, resizeIn: Boolean): Float {
        mResizeIn = resizeIn
        return getInterpolation(input)
    }
}