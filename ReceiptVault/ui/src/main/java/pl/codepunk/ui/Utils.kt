package pl.codepunk.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast

fun humanReadableByteCount(bytes: Long, si: Boolean = false): String {
    val unit = if (si) 1000 else 1024
    if (bytes < unit) return "$bytes B"
    val exp = (Math.log(bytes.toDouble()) / Math.log(unit.toDouble())).toInt()
    val pre = (if (si) "kMGTPE" else "KMGTPE")[exp - 1] + if (si) "" else "i"
    return String.format("%.1f %sB", bytes / Math.pow(unit.toDouble(), exp.toDouble()), pre)
}

fun Context.mailto(message: String, VERSION_CODE: String) {
    val intent = Intent(Intent.ACTION_SENDTO).apply {
        type = "message/rfc822"
        data = Uri.parse("mailto:")
        putExtra(Intent.EXTRA_EMAIL, arrayOf("codepunk.pl+apps@gmail.com"))
        putExtra(Intent.EXTRA_SUBJECT, "[$message] Found a problem!")
        putExtra(
            Intent.EXTRA_TEXT,
            "Hey!"
                    + "\n\nI've found an issue in the $message v${VERSION_CODE}."
                    + "\n\n{{ Please describe the problem in this section }}"
        )
    }
    try {
        startActivity(Intent.createChooser(intent, "Report a problem!"))
    } catch (ex: android.content.ActivityNotFoundException) {
        Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show()
    }
}

fun Context.playStore(APPLICATION_ID: String) {
    val intent = Intent(Intent.ACTION_VIEW).apply {
        data = Uri.parse("https://play.google.com/store/apps/details?id=$APPLICATION_ID")
        setPackage("com.android.vending")
    }
    try {
        startActivity(intent)
    } catch (ex: android.content.ActivityNotFoundException) {
        Toast.makeText(this, "There are no vending apps installed.", Toast.LENGTH_SHORT).show()
    }
}

fun Context.launch(intent: Intent) = startActivity(intent)